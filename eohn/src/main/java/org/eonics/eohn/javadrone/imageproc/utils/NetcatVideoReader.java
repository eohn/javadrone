package org.eonics.eohn.javadrone.imageproc.utils;

import java.io.IOException;
import java.io.InputStream;

public class NetcatVideoReader {

    private final Runtime runtime;
    private final int port;
    private final String ip;
    private Process process;

    NetcatVideoReader(String ip, int port) {
        this.ip = ip;
        this.port = port;
        runtime = Runtime.getRuntime();
        try {
            reconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public InputStream getDataStream() {
        return process.getInputStream();
    }

    private void reconnect() throws IOException {
        if (process != null){
            process.destroy();
        }
        process = runtime.exec("nc " +ip +" " + port);
    }
}
