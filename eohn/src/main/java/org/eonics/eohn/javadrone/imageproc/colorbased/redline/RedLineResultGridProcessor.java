/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.redline;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import org.eonics.eohn.javadrone.imageproc.helpers.ResultGrid;
import org.eonics.eohn.javadrone.imageproc.helpers.ResultGridProcessor;
import org.eonics.eohn.javadrone.imageproc.utils.GeoUtils;

/**
 *
 * @author JP
 */
public class RedLineResultGridProcessor implements ResultGridProcessor<RedLineImageData> {

    @Override
    public RedLineImageData processResultGrid(ResultGrid resultGrid) {
        if (resultGrid.getPositivePoints().isEmpty()) {
            return null;
        }

        CircleCrossings circleCrossings = new CircleCrossings(resultGrid.getCenter());
        Point center = resultGrid.getCenter();
        Point closestPoint = null;

        double minDistance = Double.MAX_VALUE;

        for(Point p: resultGrid.getPositivePoints())
        {
            double distance = GeoUtils.distance(p, center);
            if (distance < minDistance) {
                minDistance = distance;
                closestPoint = p;
            }
            circleCrossings.processPixelAt(p);
        }

        // fix for when minXCrossingPoint is calculated the same as minYXCrossingPoint
        if (minXCrossingPointExists(circleCrossings) && areMinXAndMinYIdentical(circleCrossings)) {
            circleCrossings.setMinXCrossingPoint(
                    new Point(
                        circleCrossings.getMinXCrossingPoint().x - 1,
                        circleCrossings.getMinXCrossingPoint().y - 1
                    )
            );
        }

        circleCrossings.getCrossingLine();

        return new RedLineImageData(center, closestPoint, circleCrossings);
    }

    private boolean areMinXAndMinYIdentical(CircleCrossings circleCrossings) {
        return circleCrossings.getMinXCrossingPoint() == circleCrossings.getMinYCrossingPoint();
    }

    private boolean minXCrossingPointExists(CircleCrossings circleCrossings) {
        return circleCrossings.getMinXCrossingPoint() != null;
    }
}
