/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.gui;

import com.codeminders.ardrone.NavData;
import com.codeminders.ardrone.NavDataListener;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author JP
 */
public class NavDataPanel extends JPanel implements NavDataListener {

    private final JLabel altitudeKeyPanel = new JLabel("Altitude");
    private final JTextField altitudeValuePanel = new JTextField();
    private final JLabel longitudeKeyPanel = new JLabel("Longitude");
    private final JTextField longitudeValuePanel = new JTextField();
    private final JLabel vxKeyPanel = new JLabel("vx");
    private final JTextField vxValuePanel = new JTextField();
    
    public NavDataPanel() {
        initComponents();
    }
    
    private void initComponents() {
        setLayout(new GridLayout(3,2));
        add(altitudeKeyPanel);
        add(altitudeValuePanel);
        add(longitudeKeyPanel);
        add(longitudeValuePanel);
        add(vxKeyPanel);
        add(vxValuePanel);
    }
    
    @Override
    public void navDataReceived(NavData nd) {
         altitudeValuePanel.setText(nd.getAltitude() + "");
         longitudeValuePanel.setText(nd.getLongitude() + "");
         vxValuePanel.setText(nd.getVx() + "");
    }
    
}
