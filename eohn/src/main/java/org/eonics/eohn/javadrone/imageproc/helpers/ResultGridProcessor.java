/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.helpers;

import org.eonics.eohn.javadrone.ImageData;

/**
 *
 * @author JP
 * @param <T>
 */
public interface ResultGridProcessor<T extends ImageData> {

    T processResultGrid(ResultGrid resultGrid);
}
