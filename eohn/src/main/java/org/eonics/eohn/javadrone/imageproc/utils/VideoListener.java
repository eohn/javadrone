package org.eonics.eohn.javadrone.imageproc.utils;

import java.awt.image.BufferedImage;

public interface VideoListener {
    void frameReceived(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize);
    void imageReceived(BufferedImage image);

}
