/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.utils;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eonics.eohn.javadrone.imageproc.helpers.Vector2D;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JP
 */
public class GeoUtilsTest {

    private static final Logger LOG = Logger.getLogger(GeoUtilsTest.class.getName());
    
    
    public GeoUtilsTest() {
    }

    /**
     * Test of distance method, of class GeoUtils.
     */
    @Test
    public void testDistance_Point_Point() {
    }

    /**
     * Test of distance method, of class GeoUtils.
     */
    @Test
    public void testDistance_Point2D_Point2D() {
    }
    

    /**
     * Test of calculateRadians method, of class GeoUtils.
     */
    @Test
    public void testCalculateRadians_Point2D_Point2D() {
        double upperLeftQuadrant = Math.toDegrees(
                GeoUtils.calculateRadians(new Point2D.Double(200, 200), new Point2D.Double(100, 100)));
        LOG.log(Level.INFO, "ULQ: {0}", upperLeftQuadrant);
        double upperRightQuadrant = Math.toDegrees(
                GeoUtils.calculateRadians(new Point2D.Double(200, 200), new Point2D.Double(300, 100)));
        LOG.log(Level.INFO, "URQ: {0}", upperRightQuadrant);
        double lowerRightQuadrant = Math.toDegrees(
                GeoUtils.calculateRadians(new Point2D.Double(200, 200), new Point2D.Double(300, 300)));
        LOG.log(Level.INFO, "LRQ: {0}", lowerRightQuadrant);
        double lowerLeftQuadrant = Math.toDegrees(
                GeoUtils.calculateRadians(new Point2D.Double(200, 200), new Point2D.Double(100, 300)));
        LOG.log(Level.INFO, "LLQ: {0}", lowerLeftQuadrant);
    }

    /**
     * Test of calculateRadians method, of class GeoUtils.
     */
    @Test
    public void testCalculateRadians_Line2D() {
    }

    /**
     * Test of fromPoint2D method, of class GeoUtils.
     */
    @Test
    public void testFromPoint2D() {
    }

    /**
     * Test of combineOverlaps method, of class GeoUtils.
     */
    @Test
    public void testCombineOverlaps() {
    }

    /**
     * Test of calculateAverageVector method, of class GeoUtils.
     */
    @Test
    public void testCalculateAverageVector() {
        Vector2D v1 = new Vector2D(Math.toRadians(160), 50);
        Vector2D v2 = new Vector2D(Math.toRadians(170), 70);
        Vector2D v3 = new Vector2D(Math.toRadians(180), 90);
        
        Queue<Vector2D> vQ = new ConcurrentLinkedQueue<>(); 
        vQ.addAll(Arrays.asList(v1,v2,v3));
        
        Vector2D averageVector = GeoUtils.calculateAverageVector(vQ);
        System.out.println(averageVector);
        assertTrue(averageVector.getAngleInDegrees() == 170);
        assertTrue(averageVector.getDistance()== 70);
        
    }
    
    /**
     * Test of calculateAverageVector method, of class GeoUtils.
     */
    @Test
    public void testCalculateAverageVector1() {
        Vector2D v1 = new Vector2D(Math.toRadians(200), 30);
        Vector2D v2 = new Vector2D(Math.toRadians(170), 70);
        Vector2D v3 = new Vector2D(Math.toRadians(200), 80);
        
        Queue<Vector2D> vQ = new ConcurrentLinkedQueue<>(); 
        vQ.addAll(Arrays.asList(v1,v2,v3));
        
        Vector2D averageVector = GeoUtils.calculateAverageVector(vQ);
        System.out.println(averageVector);
        assertTrue(averageVector.getAngleInDegrees() == -170);
        assertTrue(averageVector.getDistance()== 60);
        
    }

    /**
     * Test of get180DegreeAngle method, of class GeoUtils.
     */
    @Test
    public void testGet180DegreeAngle() {
    }

    /**
     * Test of get360DegreeAngle method, of class GeoUtils.
     */
    @Test
    public void testGet360DegreeAngle() {
    }
    
}
