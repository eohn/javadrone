/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.behaviour;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JP
 */
public class DroneMotionAngleMap {

    private final List<DroneMotionAngleMapEntry> angleMapEntries = new ArrayList<>();

    public void addAngleMapping(AngleMatcher angleMatcher, DroneMotion droneMotion) {
        angleMapEntries.add(new DroneMotionAngleMapEntry(angleMatcher, droneMotion));
    }

    public void executeMotion(int angle) {
        boolean matches = false;
        for (DroneMotionAngleMapEntry angleMapEntry: angleMapEntries) {
            if(angleMapEntry.angleMatcher.matches(angle)) {
                angleMapEntry.droneMotion.execute();
                matches = true;
            }
        }
        if(!matches) {
            System.out.println("WARNING: no entry for angle " + angle);
        }
    }

    private class DroneMotionAngleMapEntry {

        AngleMatcher angleMatcher;
        DroneMotion droneMotion;

        public DroneMotionAngleMapEntry(AngleMatcher angleMatcher, DroneMotion droneMotion) {
            this.angleMatcher = angleMatcher;
            this.droneMotion = droneMotion;
        }

    }
}
