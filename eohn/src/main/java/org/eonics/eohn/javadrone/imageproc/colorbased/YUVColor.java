/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased;

import java.awt.Color;

/**
 *
 * @author JP
 */
public class YUVColor {

    private int y;
    private int u;
    private int v;

    public YUVColor(int y, int u, int v) {
        this.y = y;
        this.u = u;
        this.v = v;
    }
    
    public static YUVColor fromRGB(Color c) {
        return fromRGB(c.getRed(), c.getGreen(), c.getBlue());
    }

    public static YUVColor fromRGB(double r, double g, double b) {
        double y = 0.299 * r + 0.587 * g + 0.114 * b;
        double u = 0.492 * (b - y) - 0.147 * r - 0.289 * g + 0.436 * b;
        double v = 0.877 * (r - y) + 0.615 * r - 0.515 * g - 0.1 * b;
        return new YUVColor((int) y, (int) u, (int) v);
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return the u
     */
    public int getU() {
        return u;
    }

    /**
     * @param u the u to set
     */
    public void setU(int u) {
        this.u = u;
    }

    /**
     * @return the v
     */
    public int getV() {
        return v;
    }

    /**
     * @param v the v to set
     */
    public void setV(int v) {
        this.v = v;
    }

}
