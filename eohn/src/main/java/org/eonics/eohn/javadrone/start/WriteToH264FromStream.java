package org.eonics.eohn.javadrone.start;

import org.eonics.eohn.javadrone.imageproc.utils.VideoStream;

public class WriteToH264FromStream {
    /**
     * a
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            // Start stream on RPi (using command below) before running the program!
            // raspivid -t 0 -n -lev 4.2 -w 640 -h 480 -hf -fps 15 -l -o tcp://0.0.0.0:3333
            VideoStream.streamToH264();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
