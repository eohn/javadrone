/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.matrixCodes;

import com.google.zxing.*;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import org.eonics.eohn.javadrone.ImageProcessor;

import org.eonics.eohn.javadrone.imageproc.helpers.Screen;
import org.eonics.eohn.javadrone.imageproc.utils.PaintUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Djura
 */
public class MatrixCodeImageProcessor extends ImageProcessor<MatrixCodeImageData> {

    static Logger LOGGER = LoggerFactory.getLogger(MatrixCodeImageProcessor.class);

    private Reader reader;



    public MatrixCodeImageProcessor(){
        reader = new QRCodeReader();
    }

    // TODO: Convolve might not be the best method name. Maybe it should be renamed to "process" or "processImage".
    @Override
    public BufferedImage convolve(BufferedImage in) {
        long before = System.currentTimeMillis();

        Screen screenIn = new Screen(in);

        BufferedImage out = copyBufferedImage(in);
        Screen screenOut = new Screen(out);

        MatrixCodeImageData imageData = getMatrixCodeData(out, screenOut);


        dataListeners.forEach(dataListener -> {
            dataListener.processImageData(imageData);
        });

        QRDataHandler handler = QRDataHandler.getInstance();

        if(handler.matrixPresent()){
            PaintUtils.drawCircle(screenOut.getGraphics2D(), handler.getMatrixCenter(), 15, Color.MAGENTA);
        }

        long diff = System.currentTimeMillis() - before;

        double fps = 1000 / (double) diff;

        LOGGER.debug("Image processed in {} ms. FPS: {}", diff, fps);

        return out;
    }


   private BinaryBitmap toBinaryBitmap(BufferedImage image){
        int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        RGBLuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), pixels);

        return new BinaryBitmap(new HybridBinarizer(source));

    }

    public MatrixCodeImageData getMatrixCodeData(BufferedImage image, Screen screen){
        Result result = null;
        String text = null;
        double matrixSize = -1;

        HashMap<DecodeHintType, ResultPointCallback> hints = new HashMap<DecodeHintType, ResultPointCallback>();

        //hints.put(DecodeHintType.NEED_RESULT_POINT_CALLBACK, new QRResultCallback(screen));
        try {
            result = locateQRCode(image, hints);
            LOGGER.debug(result.getText());
        } catch (FormatException e) {
            LOGGER.debug(e.getMessage());
        } catch (ChecksumException e) {
            LOGGER.debug(e.getMessage());
        } catch (NotFoundException e) {
            LOGGER.debug(e.getMessage());
        }

        Point2D center = null;

        if(result != null) {
            center = toPoint(getCenter(result.getResultPoints()));
            text = result.getText();
            matrixSize = getMatrixSize(result.getResultPoints());
        }
        MatrixCodeImageData imageData = new MatrixCodeImageData(center, text, screen.getWidth(), screen.getHeight(), matrixSize);

        return imageData;
    }

    private double getMatrixSize(ResultPoint[] resultPoints){
        return Math.sqrt(Math.pow(resultPoints[0].getX() - resultPoints[2].getX(), 2)
                + Math.pow(resultPoints[0].getY() - resultPoints[2].getY(), 2));

    }
//    /**
//     * Gets matrix code data even if matrix code couldn't be detected.
//     * @param image
//     * @return
//     */
//    public MatrixCodeImageData getInclusiveMatrixCodeData(BufferedImage image){
//        Result result = null;
//
//        BinaryBitmap bitmap = toBinaryBitmap(image);
//
//        ResultPointCallback resultCallBack = new QRResultCallback();
//
//
//
//        try {
//
//        } catch (NotFoundException e) {
//            e.printStackTrace();
//        } catch (FormatException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//
//    }

    private Result locateQRCode(BufferedImage image, Map<DecodeHintType, ?> hints) throws FormatException, ChecksumException, NotFoundException {
        BinaryBitmap bitmap = toBinaryBitmap(image);

        return reader.decode(bitmap, hints);

    }

    static BufferedImage copyBufferedImage(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    static Point toPoint(ResultPoint resultPoint){
        // TODO: Round off values instead of only int cast.
        return new Point((int)resultPoint.getX(), (int)resultPoint.getY());

    }

    static ResultPoint getCenter(ResultPoint[] points){
        float x = (points[0].getX() + points[2].getX())/2;
        float y = (points[0].getY() + points[2].getY())/2;

        return new ResultPoint(x, y);

    }



}
