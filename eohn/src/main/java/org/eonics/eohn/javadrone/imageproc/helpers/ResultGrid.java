/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.helpers;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eonics.eohn.javadrone.imageproc.utils.GeoUtils;

/**
 *
 * @author JP
 */
public class ResultGrid {

    private final int[][] grid;
    private final int width;
    private final int height;
    private List<Rectangle> colorBlobs;
    private final List<Point> positivePoints = new ArrayList<>();

    public ResultGrid(int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new int[width][height];
    }

    public Point getCenter() {
        return new Point(width / 2, height / 2);
    }

    public ResultGrid createBlobsOnlyResultGrid() {
        ResultGrid onlyBlobsResultGrid = new ResultGrid(width, height);
        onlyBlobsResultGrid.colorBlobs = colorBlobs;

        if (colorBlobs != null) {
            colorBlobs.stream().forEach((colorBlob) -> {
                for (int x = colorBlob.x; x < colorBlob.x + colorBlob.width; x++) {
                    for (int y = colorBlob.y; y < colorBlob.y + colorBlob.height; y++) {
                        if (grid[x][y] == 1) {
                            onlyBlobsResultGrid.setPositive(new Point(x, y));
                        }
                    }
                }
            });
        }

        return onlyBlobsResultGrid;
    }

    public Iterator<PointValue> getIterator() {

        return new Iterator<PointValue>() {

            int currentX = -1, currentY = 0;

            @Override
            public boolean hasNext() {
                return currentX < width - 1 || currentY < height - 1;
            }

            @Override
            public PointValue next() {
                if (currentX < width - 1) {
                    currentX++;
                } else if (currentY < height - 1) {
                    currentX = 0;
                    currentY++;
                }
                return new PointValue(currentX, currentY,
                        grid[currentX][currentY]);
            }
        };
    }

    public void setPositive(Point p) {
        grid[p.x][p.y] = 1;
        getPositivePoints().add(p);
    }

    public void setNegative(Point p) {
        grid[p.x][p.y] = 0;
    }

    public int getValue(int x, int y) {
        return grid[x][y];
    }

    public int countResultsIn(Point p, int width, int height) {
        int counter = 0;
        for (int x = p.x; x < p.x + width; x++) {
            for (int y = p.y; y < p.y + height; y++) {
                counter += getValue(x, y);
            }
        }
        return counter;
    }

    public int countResultsIn(Rectangle rectangle) {
        return countResultsIn(new Point((int) rectangle.getX(), (int) rectangle.getY()),
                (int) rectangle.getWidth(), (int) rectangle.getHeight());
    }

    public void paintOnScreen(Screen screen, Color c) {
        for (int x = 0; x < screen.getWidth(); x++) {
            for (int y = 0; y < screen.getHeight(); y++) {
                if (getValue(x, y) > 0) {
                    screen.getBufferedImage().setRGB(x, y, c.getRGB());
                }
            }
        }
    }

    public List<Rectangle> discernBlobs(int minBlobSize) {
        List<Rectangle> blobs = new ArrayList<>();
        int step = 10;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (grid[x][y] > 0 && !pointInBlobs(blobs, x, y)) {
                    int currentCount = 0;
                    int rectWidth = step;
                    int rectHeight = step;
                    int nextCount = 0;
                    while (x + rectWidth < width && y + rectHeight < height
                            && (nextCount = countResultsIn(new Rectangle(x, y, rectWidth, rectHeight))) > currentCount * 1.1) {
                        rectWidth += step;
                        currentCount = nextCount;
                    }
                    rectHeight += step;
                    while (x + rectWidth < width && y + rectHeight < height
                            && (nextCount = countResultsIn(new Rectangle(x, y, rectWidth, rectHeight))) > currentCount * 1.1) {
                        rectHeight += step;
                        currentCount = nextCount;
                    }

                    if (nextCount > minBlobSize) {
                        blobs.add(new Rectangle(x, y, rectWidth - step, rectHeight - step));
                    }
                }
            }
        }
        return GeoUtils.combineOverlaps(blobs);
    }

    private boolean pointInBlobs(List<Rectangle> blobs, int x, int y) {
        return blobs.stream().anyMatch((blob) -> (blob.contains(x, y)));
    }

    /**
     * @param minBlobSize
     * @return the colorBlobs
     */
    public List<Rectangle> getColorBlobs(int minBlobSize) {
        if (colorBlobs == null) {
            colorBlobs = discernBlobs(minBlobSize);
        }
        return colorBlobs;
    }

    /**
     * @return the positivePoints
     */
    public List<Point> getPositivePoints() {
        return positivePoints;
    }
}
