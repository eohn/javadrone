/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import org.eonics.eohn.javadrone.imageproc.helpers.ResultGrid;
import org.eonics.eohn.javadrone.imageproc.helpers.Screen;

/**
 *
 * @author JP
 */
public abstract class ColorFilter {

    protected final String key;
    protected final ColorScanner colorScanner;
    protected final ResultGrid resultGrid;

    public ColorFilter(String key, ColorScanner colorScanner, ResultGrid resultGrid) {
        this.key = key;
        this.colorScanner = colorScanner;
        this.resultGrid = resultGrid;
    }

    public void filter(Point p, Color color) {
        if (colorScanner.isColor(color)) {
            resultGrid.setPositive(p);
        } else {
            resultGrid.setNegative(p);
        }
    }

    public List<Rectangle> getColorBlobs() {
        return resultGrid.getColorBlobs(100);
    }

    public void paintColorBlobs(Screen screen, Color color) {
        List<Rectangle> blobs = resultGrid.getColorBlobs(100);
        Graphics graphics = screen.getBufferedImage().getGraphics();
        for (Rectangle blob : blobs) {
            graphics.setColor(color);
            graphics.drawRect(blob.x, blob.y, blob.width, blob.height);
        }
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @return the colorScanner
     */
    public ColorScanner getColorScanner() {
        return colorScanner;
    }

    /**
     * @return the resultGrid
     */
    public ResultGrid getResultGrid() {
        return resultGrid;
    }

}
