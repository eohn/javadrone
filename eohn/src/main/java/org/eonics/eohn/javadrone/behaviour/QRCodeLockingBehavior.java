package org.eonics.eohn.javadrone.behaviour;

import org.eonics.eohn.javadrone.config.DroneConfig;
import org.eonics.eohn.javadrone.dataproc.StateDataHandler;
import org.eonics.eohn.javadrone.imageproc.matrixCodes.QRDataHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.geom.Point2D;

public class QRCodeLockingBehavior implements MovementBehaviour {

    static Logger LOGGER = LoggerFactory.getLogger(QRCodeLockingBehavior.class);
    private static final Point2D DEFAULT_DESIRED_QR_POSITION = new Point2D.Double(0.5, 0.5);
    private static final double VERTICAL_VELOCITY = 0.05F;
    private static final double LATERAL_VELOCITY = 0.05F;
    private static final double RELATIVE_MARGIN = 0.001;

    /***
     * The desired position the QR code should be in on the camera image. To keep this value screen resolution
     * independent it is indicated in values between 0 and 1 and should be multiplied by screen resolution to get the
     * actual desired position in the camera image.
     */
    private Point2D relativeDesiredPosition;

    public QRCodeLockingBehavior(){
        this(DEFAULT_DESIRED_QR_POSITION);
    }

    public QRCodeLockingBehavior(Point2D relativeDesiredPosition){
        this.relativeDesiredPosition = relativeDesiredPosition;
    }

    /***
     * Computes the difference between current QR position and desired position. Move a little bit in the direction of
     * the difference at a fixed speed.
     * @return
     */
    public Movement query() {

        QRDataHandler qrState = QRDataHandler.getInstance();

        if (qrState.matrixPresent()) {

            // TODO: This should not be computed every time, only once
            Point2D desiredPosition = new Point2D.Double(relativeDesiredPosition.getX() * (double) qrState.getScreenWidth(),
                    relativeDesiredPosition.getY() * (double) qrState.getScreenHeight());

            Point2D matrixCenter = qrState.getMatrixCenter();

            Point2D diff = new Point2D.Double(matrixCenter.getX() - desiredPosition.getX(),
                    matrixCenter.getY() - desiredPosition.getY());

            double verticalMovement = 0;
            double lateralMovement = 0;


            // y axis in image goes from high to low. Vertical velocity from low to high
            if(Math.abs(diff.getY()/(double)qrState.getScreenHeight()) > RELATIVE_MARGIN)
                verticalMovement = -1 * VERTICAL_VELOCITY * (Double) Math.signum(diff.getY());


            if(Math.abs(diff.getX()/(double)qrState.getScreenWidth()) > RELATIVE_MARGIN)
                lateralMovement = LATERAL_VELOCITY * (Double) Math.signum(diff.getX());


            LOGGER.info("Vertical movement:\n" + fancyVerticalMovementString(verticalMovement));
            LOGGER.info("Lateral movement: " + fancyLateralMovementString(lateralMovement));


            return new Movement(lateralMovement, 0, verticalMovement, 0);
        }
        return new Movement();

    }

    private String fancyVerticalMovementString(double movement) {

        if (movement > 0)
            return "^\n|";
        else if (movement < 0)
            return "|\nv";
        else
            return "";
    }

    private String fancyLateralMovementString(double movement) {

        if (movement > 0)
            return "-->";
        else if (movement < 0)
            return "<--";
        else
            return "";
    }
}
