package org.eonics.eohn.javadrone.start;

import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.DroneMovementController;
import org.eonics.eohn.javadrone.DroneQRMovementController;
import org.eonics.eohn.javadrone.gui.ControlPanel;
import org.eonics.eohn.javadrone.gui.DroneDashboard;
import org.eonics.eohn.javadrone.gui.PiDroneDashboard;
import org.eonics.eohn.javadrone.imageproc.matrixCodes.MatrixCodeImageProcessor;
import org.eonics.eohn.javadrone.imageproc.matrixCodes.QRDataHandler;
import org.eonics.eohn.javadrone.imageproc.utils.ImgProcUtils;
import org.eonics.eohn.javadrone.imageproc.utils.VideoStream;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DroneLockQRCode {

    static Logger LOGGER = LoggerFactory.getLogger(DroneLockQRCode.class);

    private static final long CONNECT_TIMEOUT = 10000;

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        try {
            ARDrone drone = new ARDrone();
            //ARDrone drone = new ARDrone(InetAddress.getByName("192.168.0.101"), 1000, 1000);
            LOGGER.info("Connecting...");
            drone.connect();
            LOGGER.info("Connected.");
            drone.clearEmergencySignal();

            drone.selectVideoChannel(ARDrone.VideoChannel.HORIZONTAL_ONLY);

            drone.waitForReady(CONNECT_TIMEOUT);

            drone.trim();

            LOGGER.info("Taking off");
            drone.takeOff();

            MatrixCodeImageProcessor matrixCodeImageProcessor = new MatrixCodeImageProcessor();
            matrixCodeImageProcessor.addDataListener(QRDataHandler.getInstance());

            DroneDashboard droneDashboard = new DroneDashboard(matrixCodeImageProcessor);
            droneDashboard.setDrone(drone);

            // Start stream on RPi (using command below) before running the program!
            // raspivid -t 0 -n -lev 4.2 -w 640 -h 480 -hf -fps 15 -l -o tcp://0.0.0.0:3333
            //VideoStream.streamToImageView("192.168.0.102", 3333, 0, "h264", 15, 1000, "ultrafast", 0);

            new ControlPanel(drone);

            Thread.sleep(5000);

            new DroneQRMovementController(drone);

            Mat frame = new Mat();

            long currentTimeMillis = System.currentTimeMillis();

            while (System.currentTimeMillis() - currentTimeMillis < 1000000){
                BufferedImage mat2Img = ImgProcUtils.mat2Img(frame);

                if(mat2Img != null){
                    droneDashboard.getVideoPanel().imageReceived((mat2Img));
                    droneDashboard.getConvolvingVideoPanel().imageReceived(mat2Img);
                }
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
