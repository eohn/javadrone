package org.eonics.eohn.javadrone.behaviour;

import org.eonics.eohn.javadrone.dataproc.StateDataHandler;

public class LandMovementBehaviour implements MovementBehaviour {

    public Movement query() {
        Movement movement = new Movement(0, 0, 0, 0);

        if (StateDataHandler.getInstance().shouldLand()) {
            movement.setVerticalVelocity(Float.NEGATIVE_INFINITY);
        }

        return movement;
    }
}
