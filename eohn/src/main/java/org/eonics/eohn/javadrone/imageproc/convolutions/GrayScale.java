/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.convolutions;

import org.eonics.eohn.javadrone.ImageProcessor;
import java.awt.Color;
import java.awt.image.BufferedImage;
import org.opencv.core.Mat;

/**
 *
 * @author JP
 */
public class GrayScale extends ImageProcessor {

    @Override
    public BufferedImage convolve(BufferedImage in) {

        int width = in.getWidth();
        int height = in.getHeight();

        BufferedImage out = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);

        for (int i = 0; i < height; i++) {

            for (int j = 0; j < width; j++) {

                Color c = new Color(in.getRGB(j, i));
                int red = (int) (c.getRed() / 3);
                int green = (int) (c.getGreen() / 3);
                int blue = (int) (c.getBlue() / 3);
                Color newColor = new Color(red + green + blue,
                        red + green + blue, red + green + blue);

                out.setRGB(j, i, newColor.getRGB());
            }
        }
        return out;
    }

}
