package org.eonics.eohn.javadrone.behaviour;

import org.eonics.eohn.javadrone.imageproc.matrixCodes.QRDataHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QRDistanceBehavior implements MovementBehaviour {

    static Logger LOGGER = LoggerFactory.getLogger(QRCodeLockingBehavior.class);
    private static final double DESIRED_RELATIVE_MATRIX_SIZE = 0.1;
    private static final double RELATIVE_MARGIN = 0.01;
    private static final double LONGITUDAL_SPEED = 0.05f;



    @Override
    public Movement query() {
        QRDataHandler qrState = QRDataHandler.getInstance();

        if(qrState.matrixPresent()){
            double matrixSize = qrState.getMatrixSize();

            LOGGER.debug("Matrix size: {}", matrixSize);

            double relativeSize = matrixSize / (double)qrState.getScreenWidth();

            LOGGER.debug("Relative matrix size: {}", relativeSize);

            double diff = relativeSize - DESIRED_RELATIVE_MATRIX_SIZE;


            if( diff < 0 - RELATIVE_MARGIN){
                // Drone is too far away
                LOGGER.info("MOVE CLOSER");
                return new Movement(0, -1 * LONGITUDAL_SPEED, 0, 0);
            }

            else if (diff > 0 + RELATIVE_MARGIN){
                // Drone is too close
                LOGGER.info("MOVE AWAY");
                return new Movement(0, LONGITUDAL_SPEED, 0, 0);
            }

            return new Movement();


        }
        return new Movement();
    }
}
