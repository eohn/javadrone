/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.behaviour;

import com.codeminders.ardrone.ARDrone;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JP
 */
public class DroneMotion {

    private final ARDrone drone;
    private final DroneAction droneAction;
    private final float speed;

    public DroneMotion(ARDrone drone, DroneAction droneAction, float speed) {
        this.drone = drone;
        this.droneAction = droneAction;
        this.speed = speed;
    }

    public void execute() {
    	
        System.out.println(droneAction.name());
    	
        try {
        	
            if (drone != null) {
                switch (droneAction) {
                    case MOVE_FORWARD:
                        System.out.println("Forward");
                        drone.move(0, -1 * speed, 0, 0);
                        break;
                    case MOVE_BACKWARD:
                        System.out.println("Backward");
                        drone.move(0, speed, 0, 0);
                        break;
                    case MOVE_LEFT:
                        System.out.println("Move left");
                        drone.move(-1 * speed, 0, 0, 0);
                        break;
                    case MOVE_RIGHT:
                        System.out.println("Move right");
                        drone.move(speed, 0, 0, 0);
                        break;
                    case TURN_LEFT:
                        System.out.println("Turn left");
                        drone.move(0, 0, 0, -1 * speed);
                        break;
                    case TURN_RIGHT:
                        System.out.println("Turn right");
                        drone.move(0, 0, 0, speed);
                        break;
                    case MOVE_DOWN:
                        System.out.println("Move down");
                        drone.move(0, 0, -1 * speed, 0);
                        break;
                    case MOVE_UP:
                        System.out.println("Move up");
                        drone.move(0, 0, speed, 0);
                        break;
                    case LAND:
                        System.out.println("Land");
                        drone.land();
                        break;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(DroneMotion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
