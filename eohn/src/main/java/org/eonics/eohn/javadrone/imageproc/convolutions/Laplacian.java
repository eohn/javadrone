/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.convolutions;

import org.eonics.eohn.javadrone.OpenCvConvolution;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author JP
 */
public class Laplacian extends OpenCvConvolution {

    @Override
    public Mat convolve(Mat in) {
        Mat out = new Mat(in.rows(), in.cols(), in.type());
        Imgproc.Laplacian(in, out, -1);
        return out;
    }

}
