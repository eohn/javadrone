/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;

/**
 *
 * @author JP
 */
public class PaintUtils {

    public static void drawCircle(Graphics2D g2d, Point p, int r, Color c) {
        g2d.setColor(c);
        if (p != null) {
            g2d.fillOval(p.x - r / 2, p.y - r / 2, r, r);
        }
    }

    public static void drawCircle(Graphics2D g2d, Point2D p, int r, Color c) {
        drawCircle(g2d, new Point((int) p.getX(), (int) p.getY()), r, c);
    }
}
