package org.eonics.eohn.javadrone.behaviour;

public class Movement {
    private double lateralTilt, longitudinalTilt,  verticalVelocity, angularVelocity;

    public Movement(double lateralTilt, double longitudinalTilt, double verticalVelocity, double angularVelocity) {
        this.lateralTilt = lateralTilt;
        this.longitudinalTilt = longitudinalTilt;
        this.verticalVelocity = verticalVelocity;
        this.angularVelocity = angularVelocity;
    }

    public Movement() {

    }

    public double getLateralTilt() {
        return lateralTilt;
    }

    public void setLateralTilt(double lateralTilt) {
        this.lateralTilt = lateralTilt;
    }

    public double getLongitudinalTilt() {
        return longitudinalTilt;
    }

    public void setLongitudinalTilt(double longitudinalTilt) {
        this.longitudinalTilt = longitudinalTilt;
    }

    public double getVerticalVelocity() {
        return verticalVelocity;
    }

    public void setVerticalVelocity(double verticalVelocity) {
        this.verticalVelocity = verticalVelocity;
    }

    public double getAngularVelocity() {
        return angularVelocity;
    }

    public void setAngularVelocity(double angularVelocity) {
        this.angularVelocity = angularVelocity;
    }

    public static Movement combine(Movement movement, Movement movement1) {
        Movement res = new Movement();

        res.setLateralTilt(movement.getLateralTilt() + movement1.getLateralTilt());
        res.setAngularVelocity(movement.getAngularVelocity() + movement1.getAngularVelocity());
        res.setLongitudinalTilt(movement.getLongitudinalTilt() + movement1.getLongitudinalTilt());
        res.setVerticalVelocity(movement.getVerticalVelocity() + movement1.getVerticalVelocity());
        return res;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Movement{");
        sb.append("lateralTilt=").append(lateralTilt);
        sb.append(", longitudinalTilt=").append(longitudinalTilt);
        sb.append(", verticalVelocity=").append(verticalVelocity);
        sb.append(", angularVelocity=").append(angularVelocity);
        sb.append('}');
        return sb.toString();
    }
}
