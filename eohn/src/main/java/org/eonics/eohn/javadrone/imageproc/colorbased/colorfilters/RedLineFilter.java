/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.colorfilters;

import java.util.logging.Logger;
import org.eonics.eohn.javadrone.imageproc.colorbased.ColorFilter;
import org.eonics.eohn.javadrone.imageproc.helpers.ResultGrid;
import org.eonics.eohn.javadrone.imageproc.colorbased.colorscanners.RedColorScanner;

/**
 *
 * @author JP
 */
public class RedLineFilter extends ColorFilter {

    private static final Logger LOG = Logger.getLogger(RedLineFilter.class.getName());

    private static final int RADIUS = 100;
    private static final int VARIANCE = 5;

    public RedLineFilter(ResultGrid resultGrid) {
        super("redline", new RedColorScanner(), resultGrid);
    }
}
