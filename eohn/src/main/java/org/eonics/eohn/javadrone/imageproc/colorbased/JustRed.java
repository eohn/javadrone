/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased;

import org.eonics.eohn.javadrone.imageproc.helpers.Screen;
import org.eonics.eohn.javadrone.imageproc.helpers.ResultGrid;
import org.eonics.eohn.javadrone.imageproc.helpers.PointsAverage;
import org.eonics.eohn.javadrone.imageproc.colorbased.ColorScanner;
import org.eonics.eohn.javadrone.imageproc.colorbased.ColorFilter;
import org.eonics.eohn.javadrone.ImageProcessor;
import com.asprise.ocr.Ocr;
import com.codeminders.ardrone.ARDrone;
import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eonics.eohn.javadrone.imageproc.colorbased.colorfilters.OrangeBeamFilter;
import org.eonics.eohn.javadrone.imageproc.colorbased.colorfilters.RedLineFilter;
import org.eonics.eohn.javadrone.imageproc.colorbased.colorfilters.YellowStickerFilter;

/**
 *
 * @author JP
 */
public class JustRed extends ImageProcessor {

    private static final Logger LOG = Logger.getLogger(JustRed.class.getName());

    private final ARDrone drone;

    public JustRed(ARDrone drone) {
        this.drone = drone;
    }

    @Override
    public BufferedImage convolve(BufferedImage in) {

        Screen screenIn = new Screen(in);

        BufferedImage out = new BufferedImage(screenIn.getWidth(), screenIn.getHeight(), BufferedImage.TYPE_INT_RGB);
        Screen screenOut = new Screen(out);

        RedLineFilter redlineFilter = new RedLineFilter(
                new ResultGrid(screenIn.getWidth(), screenIn.getHeight()));
        OrangeBeamFilter orangeBeamFilter = new OrangeBeamFilter(
                new ResultGrid(screenIn.getWidth(), screenIn.getHeight()));
        YellowStickerFilter yellowStickerFilter = new YellowStickerFilter(
                new ResultGrid(screenIn.getWidth(), screenIn.getHeight()));

        List<ColorFilter> colorFilters = Arrays.asList(redlineFilter);

//        screenIn.filter(colorFilters);
//       
//        redlineFilter.paintColorOnScreen(screenOut, Color.red);
//        redlineFilter.drawCrossings(screenOut);
//        redlineFilter.drawAngleLine(screenOut);

//        redlineFilter.paintOnScreen(screenOut, Color.RED);
//        orangeBeamFilter.paintColorOnScreen(screenOut, Color.ORANGE);
//        orangeBeamFilter.paintColorBlobs(screenOut, Color.ORANGE);
//        yellowStickerFilter.paintColorOnScreen(screenOut, Color.YELLOW);
//        yellowStickerFilter.paintColorBlobs(screenOut, Color.YELLOW);
//        List<Rectangle> orangeBlobs = orangeBeamFilter.getColorBlobs();
//        List<Rectangle> yellowBlows = yellowStickerFilter.getColorBlobs();
//
//        int maxOrange = 0;
//        int yAtMaxOrange = -1;
//        int beamHeight = 80;
//
//        List<Rectangle> beams = new ArrayList<>();
//
//        for (int i = 0; i < screenOut.getHeight(); i += 5) {
//
//            Set<Rectangle> matchingOrangeBlobs = new HashSet<>();
//            Set<Rectangle> matchingYellowBlobs = new HashSet<>();
//
//            Rectangle beam = new Rectangle(0, i, screenOut.getWidth(), beamHeight);
//
//            for (Rectangle orangeBlob : orangeBlobs) {
//                if (beam.contains(orangeBlob)) {
//                    matchingOrangeBlobs.add(orangeBlob);
//                }
//            }
//
//            for (Rectangle yellowBlob : yellowBlows) {
//                if (beam.contains(yellowBlob)) {
//                    matchingYellowBlobs.add(yellowBlob);
//                }
//            }
//
//            if (matchingOrangeBlobs.size() > 5 && matchingYellowBlobs.size() > 1) {
//                beams.add(beam);
//            }
//        }
//
//        List<Rectangle> combinedBeams = GeoUtils.combineOverlaps(beams);
//
//        for (Rectangle beam : combinedBeams) {
//            screenOut.getGrapics2D().drawRect(beam.x, beam.y, beam.width, beam.height);
//
//            int stickerWidth = 300;
//            int stickerHeight = 50;
//            int minX = 0;
//            for (int j = beam.y; j + stickerHeight < beam.y + beamHeight; j += 2) {
//                for (int i = minX; i + stickerWidth < screenOut.getWidth(); i += 2) {
//
//                    Rectangle leftCorner = new Rectangle(i, j, 2, 2);
//                    boolean found = false;
//                    for (Rectangle yellowBlob : yellowBlows) {
//                        if (leftCorner.intersects(yellowBlob)) {
//                            found = true;
//                            break;
//                        }
//                    }
//                    if (found) {
//                        Rectangle sticker = new Rectangle(i, j, stickerWidth, stickerHeight);
//                        Set<Rectangle> intersectingBlobs = new HashSet<Rectangle>();
//                        for (Rectangle yellowBlob : yellowBlows) {
//                            if (sticker.intersects(yellowBlob) || sticker.contains(yellowBlob)) {
//                                intersectingBlobs.add(yellowBlob);
//                            }
//                        }
//
//                        Rectangle union = null;
//                        for (Rectangle intersectingBlob : intersectingBlobs) {
//                            if (union == null) {
//                                union = intersectingBlob;
//                            } else {
//                                union = union.union(intersectingBlob);
//                            }
//                        }
//
//                        if (union != null) {
////                            System.out.println(intersectingBlobs.size());
////                            out.getGraphics().drawRect(union.x, union.y, union.width, union.height);
//
//                            if (union.width > (0.8 * stickerWidth) && union.width < (1.2 * stickerWidth)) {
//
//                                BufferedImage stickerImage = new BufferedImage(union.width, union.height / 2, BufferedImage.TYPE_INT_RGB);
//                                for (int q = i; q < i + union.width - 1 && q < screenOut.getWidth(); q++) {
//                                    for (int r = j; r  < j + (union.height / 2) - 1 && r < screenOut.getHeight(); r++) {
//                                        stickerImage.setRGB(q - i, r - j, in.getRGB(q, r));
//                                        out.setRGB(q, r, in.getRGB(q, r));
//                                    }
//                                }
//
//////                            out.getGraphics().drawRect(i, j, stickerWidth, stickerHeight);
//                                ocr(stickerImage);
//                                i = i + union.width;
//                                minX = i + union.width;
////                            }
//                            }
//                        }
//                    }
//                }
//            }
//        }

//        if (yAtMaxOrange > -1) {
//
//            int stickerWidth = 300;
//            int stickerHeight = 50;
//            int minX = 0;
//            for (int j = yAtMaxOrange; j + stickerHeight < yAtMaxOrange + beamHeight; j += 2) {
//                for (int i = minX; i + stickerWidth < screenOut.getWidth(); i += 2) {
//                    if (new Color(screenOut.getBufferedImage().getRGB(i, j)).equals(Color.YELLOW)) {
//                        if (getNumberOfColorPixels(screenOut, i, j, stickerWidth, stickerHeight, Color.YELLOW) > 1000) {
//                            in.getGraphics().drawRect(i, j, stickerWidth, stickerHeight);
//                            out.getGraphics().drawRect(i, j, stickerWidth, stickerHeight);
//                            
//                            i = i + stickerWidth;
//                            minX = i + stickerWidth;
//                        }
//                    }
//                }
//            }
//
//            in.getGraphics().drawRect(0, yAtMaxOrange, out.getWidth(), beamHeight);
//        }
//        drawAngleLine(screenOut, pMinGX, new Point[]{pMinGX, pMaxGX, pMinGY, pMaxGY});
//        drawCrossings(screenOut, xCrossings, yCrossings);
//
//        moveDrone(redPixels, screenIn);
        return out;
    }

    private BufferedImage revertAndUnify(BufferedImage bi) {
        BufferedImage biOut = new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        for (int x = 0; x < bi.getWidth(); x++) {
            for (int y = 0; y < bi.getHeight(); y++) {
                if (bi.getRGB(x, y) < 100) {
                    biOut.setRGB(x, y, 255);
                } else {
                    biOut.setRGB(x, y, 0);
                }
            }
        }
        return biOut;
    }

    private void ocr(BufferedImage bi) {
        Ocr.setUp(); // one time setup
        Ocr ocr = new Ocr(); // create a new OCR engine
        ocr.startEngine("eng", Ocr.SPEED_FASTEST); // English
        String s = ocr.recognize(bi, Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
        System.out.println("Result: " + s);
// ocr more images here ...
        ocr.stopEngine();

    }

    private int getNumberOfColorPixels(Screen screen, int x, int y, int width, int height, Color c) {
        int matches = 0;
        for (int i = x; i < x + width; i++) {
            for (int j = y; j < y + height; j++) {
                if (i < screen.getWidth() && j < screen.getHeight()) {
                    if (new Color(screen.getBufferedImage().getRGB(i, j)).equals(c)) {
                        matches++;
                    }
                }
            }
        }
        return matches;
    }

    private boolean scanForColor(Screen screen, Point p, int reach, ColorScanner c) {
        for (int x = p.x - reach; x < p.x + reach; x++) {
            for (int y = p.y - reach; y < p.y + reach; y++) {
                if (x >= 0 && x < screen.getWidth() && y >= 0 && y < screen.getHeight()) {
                    if (c.isColor(new Color(screen.getBufferedImage().getRGB(x, y)))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void moveDrone(PointsAverage redPixels, Screen screen) {

        int count = redPixels.getCountX();

        if (redPixels.getCountX() > 0) {

            Point average = redPixels.getAverage();

            LOG.log(Level.INFO, "Found {0} red pixels", count);
            if (count > 250) {

                try {
                    boolean forward = false;
                    if (average.y < screen.getHeight() / 3) {
                        LOG.info("DOWN");
                        if (drone != null) {
                            drone.move(0, 0, 0.2F, 0);
                        }
                    } else if (average.y > 2 * screen.getHeight() / 3) {
                        LOG.info("UP");
                        if (drone != null) {
                            drone.move(0, 0, -0.2F, 0);
                        }
                    } else {
                        forward = true;
                    }
                    if (average.x < (screen.getWidth() / 3)) {
                        LOG.info("LEFT");
                        if (drone != null) {
                            drone.move(0, 0, 0, -0.2F);
                        }

                    } else if (average.x > (2 * screen.getWidth() / 3)) {
                        LOG.info("RIGHT");
                        if (drone != null) {
                            drone.move(0, 0, 0, 0.2F);

                        }
                    }
//                        else if (forward) {
//                            LOG.info("FORWARD");
//                            if (drone != null) {
//                                drone.move(0, -0.1F, 0, 0);
//                            }
//                        }
                } catch (IOException ex) {
                    Logger.getLogger(JustRed.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
