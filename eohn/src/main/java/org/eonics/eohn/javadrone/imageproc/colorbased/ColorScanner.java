/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased;

import java.awt.Color;

/**
 *
 * @author JP
 */
public interface ColorScanner {
        
    boolean isColor(Color c);
}
