package org.eonics.eohn.javadrone.start;

import org.eonics.eohn.javadrone.gui.DroneDashboard;
import org.eonics.eohn.javadrone.imageproc.colorbased.redline.RedLineImageProcessor;
import org.eonics.eohn.javadrone.imageproc.utils.VideoStream;
import org.opencv.core.Core;

public class RedFilteringFromStream {

    private static final long PROCESSING_TIMEOUT = 60000;

    /**
     * a
     *
     * @param args
     */
    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        try {

            RedLineImageProcessor redLineImageProcessor = new RedLineImageProcessor();
            DroneDashboard droneDashboard = new DroneDashboard(redLineImageProcessor);

            // Start stream on RPi (using command below) before running the program!
            // raspivid -t 0 -n -lev 4.2 -w 640 -h 480 -hf -fps 15 -l -o tcp://0.0.0.0:3333
            VideoStream.streamToImageView("192.168.0.102", 3333, 0, "h264", 15, 1000, "ultrafast", 0);

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
