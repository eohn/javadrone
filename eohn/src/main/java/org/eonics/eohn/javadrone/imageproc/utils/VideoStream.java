package org.eonics.eohn.javadrone.imageproc.utils;

import com.twilight.h264.util.IOUtils;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class VideoStream {

    private static List<VideoListener> listeners = new ArrayList<>();

    private VideoStream() {
    }

    public static void addDroneVideoListener(VideoListener l) {
        listeners.add(l);
    }

    public static void streamToH264() {
        NetcatVideoReader videoReader = new NetcatVideoReader("192.168.1.137", 3333);

        InputStream initialStream = videoReader.getDataStream();
        File targetFile = new File("/home/opalampo/Videos/test.mp4");

        try {
            OutputStream outStream = new FileOutputStream(targetFile);

            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = initialStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            IOUtils.closeQuietly(initialStream);
            IOUtils.closeQuietly(outStream);
        } catch (FileNotFoundException $e) {
            System.out.println("File not found");
        } catch (IOException $e) {
            System.out.println("IOException");
        }
    }

    public static void streamToImageView (
            final String ip,
            final int port,
            final int socketBacklog,
            final String format,
            final double frameRate,
            final int bitrate,
            final String preset,
            final int numBuffers
    ) {

        try {

            NetcatVideoReader videoReader = new NetcatVideoReader(ip, port);

            final FrameGrabber grabber = new FFmpegFrameGrabber(videoReader.getDataStream());
            final Java2DFrameConverter converter = new Java2DFrameConverter();

            grabber.setFrameRate(frameRate);
            grabber.setFormat(format);
            grabber.start();

            while (!Thread.interrupted()) {
                final Frame frame = grabber.grab();

                if (frame != null) {
                    final BufferedImage bufferedImage = converter.convert(frame);

                    if (bufferedImage != null) {
                        listeners.parallelStream().forEach(listener -> listener.imageReceived(bufferedImage));
                    }
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}

