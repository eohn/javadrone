/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone;

import com.codeminders.ardrone.ARDrone;

/**
 *
 * @author JP
 */
public abstract class Behaviour {

    protected final ARDrone drone;

    public Behaviour(ARDrone drone) {
        this.drone = drone;
    }

    public abstract void start();

    public abstract void stop();

}
