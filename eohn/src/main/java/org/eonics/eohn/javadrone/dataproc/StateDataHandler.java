package org.eonics.eohn.javadrone.dataproc;

import org.eonics.eohn.javadrone.ImageProcessorDataListener;
import org.eonics.eohn.javadrone.imageproc.colorbased.redline.RedLineImageData;
import org.eonics.eohn.javadrone.imageproc.helpers.Vector2D;
import org.eonics.eohn.javadrone.imageproc.utils.GeoUtils;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StateDataHandler implements ImageProcessorDataListener<RedLineImageData> {
    private static StateDataHandler ourInstance;// = new StateDataHandler();

    public static StateDataHandler getInstance() {
        if (ourInstance == null) {
            ourInstance = new StateDataHandler();
        }

        return ourInstance;
    }

    private StateDataHandler() {
    }

    private double angle = Double.NaN;
    private double vectorAngle = Double.NaN;
    private double lineAngle = Double.NaN;
    private double distance;

    private Vector2D vectorToClosestPoint;

    private Vector2D[] angleQueue = new Vector2D[3];
    private int angleIndex = 0;

    private boolean land = false;

    private long lastDataFrame = System.currentTimeMillis();

    public static final double MAX_LINE_LOCK_THRESHOLD = 60;

    public static final double MIN_PERPENDICULAR_ANGLE = 83;
    public static final double MAX_PERPENDICULAR_ANGLE = 97;

    // If the drone does not see the red line it lands after this timeframe
    public static final long NO_LOCATION_DATA_LAND_TIMEOUT = 1000000; // milliseconds

    @Override
    public void processImageData(RedLineImageData data) {

        if (data == null) {
            angle = Double.NaN;

            if (System.currentTimeMillis() > lastDataFrame + NO_LOCATION_DATA_LAND_TIMEOUT) {
                land = true;
            }

            return;
        } else {
            lastDataFrame = System.currentTimeMillis();
        }

        vectorToClosestPoint = data.getVectorToClosestPoint();

        if (vectorToClosestPoint != null) {
            addVectorToAngleQueue(vectorToClosestPoint);
            Vector2D averageVector = calculateAverageVector();

            vectorAngle = getAverageAngle(averageVector);
            distance = getAverageDistance(averageVector);
        }

        if (data.getLineAngleInDegrees() != null) {
            lineAngle = data.getLineAngleInDegrees();
        }

        if (distance < 10) {
            /*temp solution because line angle jumps from -180 to +180
            when drone is perpendicular to the line*/
            angle = Math.abs(lineAngle) - 90;
        } else {
            angle = vectorAngle;
        }

        System.out.println("Angle: " + angle);
        System.out.println("Distance: " + distance);
    }

    public boolean isDroneOnLine() {
        return distance < MAX_LINE_LOCK_THRESHOLD;
    }

    public boolean isDroneLockedOnLine() {
        return isDroneOnLine() && isDronePerpendicularToLine();
    }

    public boolean isDronePerpendicularToLine() {
        if (!Double.isNaN(angle)) {

            boolean perpendicularAtBackOfLine = angle > MIN_PERPENDICULAR_ANGLE && angle < MAX_PERPENDICULAR_ANGLE;
            boolean perpendicularAtFrontOfLine = angle < - MIN_PERPENDICULAR_ANGLE && angle > - MAX_PERPENDICULAR_ANGLE;

            return perpendicularAtBackOfLine || perpendicularAtFrontOfLine;
        }

        return false;
    }

    public double getAngle() {
        return angle;
    }

    public double getDistance() {
        return distance;
    }

    public Vector2D getVectorToClosestPoint() {
        return vectorToClosestPoint;
    }

    private void addVectorToAngleQueue(Vector2D vector) {
        angleQueue[angleIndex] = vector;
        angleIndex++;
        if(angleIndex >= angleQueue.length){
            angleIndex = 0;
        }
    }

    private Vector2D calculateAverageVector() {
        return GeoUtils.calculateAverageVector(Stream.of(angleQueue).filter(Objects::nonNull).collect(Collectors.toList()));
    }

    private double getAverageAngle(Vector2D averageVector) {
        return averageVector.getAngleInDegrees();
    }

    private double getAverageDistance(Vector2D averageVector) {
        return averageVector.getDistance();
    }

    public boolean shouldLand() {
        return land;
    }
}
