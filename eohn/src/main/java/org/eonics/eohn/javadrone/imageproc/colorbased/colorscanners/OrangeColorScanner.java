/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.colorscanners;

import java.awt.Color;
import org.eonics.eohn.javadrone.imageproc.colorbased.ColorScanner;
import org.eonics.eohn.javadrone.imageproc.utils.ColorUtils;

/**
 *
 * @author JP
 */
public class OrangeColorScanner implements ColorScanner{

    @Override
    public boolean isColor(Color color) {
        int hue = ColorUtils.getHue(color);
        return (hue > 0 && hue < 2.5) || (color.getRed() > 60 && color.getBlue() < 47);
    }
    
}
