package org.eonics.eohn.javadrone.imageproc.matrixCodes;

import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import org.eonics.eohn.javadrone.imageproc.helpers.Screen;
import org.eonics.eohn.javadrone.imageproc.utils.PaintUtils;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

public class QRResultCallback implements ResultPointCallback {

    Screen screen;
    Graphics2D g2d;

    public QRResultCallback(Screen screen){
        this.screen = screen;
        g2d = screen.getGraphics2D();

    }

    @Override
    public void foundPossibleResultPoint(ResultPoint resultPoint) {
        Point2D point = MatrixCodeImageProcessor.toPoint(resultPoint);

        PaintUtils.drawCircle(g2d, point, 15, Color.RED);
    }


}
