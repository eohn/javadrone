/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.colorscanners;

import java.awt.Color;
import org.eonics.eohn.javadrone.imageproc.colorbased.YUVColor;
import org.eonics.eohn.javadrone.imageproc.colorbased.ColorScanner;

/**
 *
 * @author JP
 */
public class RedColorScanner implements ColorScanner{

    @Override
    public boolean isColor(Color color) {
        YUVColor yuv = YUVColor.fromRGB(color);
        
        return yuv.getV() > 50 && yuv.getV() < 200;
    }
    
}
