/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.convolutions;

import org.eonics.eohn.javadrone.OpenCvConvolution;
import static java.lang.System.out;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author JP
 */
public class GaussianBlur extends OpenCvConvolution {
    
    private int r;

    public GaussianBlur(int r) {
        this.r = r;
    }
    
    

    @Override
    public Mat convolve(Mat in) {
        Mat out = new Mat(in.rows(), in.cols(), in.type());
        Imgproc.GaussianBlur(in, out, new Size(r, r), 0);

        return out;
    }

}
