/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.utils;

import java.awt.Color;

/**
 *
 * @author JP
 */
public final class ColorUtils {
    
    public static int getHue(Color color) {
        float[] hsv = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsv);
        return (int) (100 * hsv[0]);
    }
}
