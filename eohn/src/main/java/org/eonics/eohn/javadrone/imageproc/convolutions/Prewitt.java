/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.convolutions;

import org.eonics.eohn.javadrone.OpenCvConvolution;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author JP
 */
public class Prewitt extends OpenCvConvolution {

    private static final int KERNEL_SIZE = 9;

    @Override
    public Mat convolve(Mat in) {
        Mat kernel = new Mat(KERNEL_SIZE, KERNEL_SIZE, CvType.CV_32F) {
            {
                put(0, 0, -1);
                put(0, 1, 0);
                put(0, 2, 1);

                put(1, 0 - 1);
                put(1, 1, 0);
                put(1, 2, 1);

                put(2, 0, -1);
                put(2, 1, 0);
                put(2, 2, 1);
            }
        };
        Mat out = new Mat(in.rows(), in.cols(), in.type());
        Imgproc.filter2D(in, out, -1, kernel);
        return out;
    }

}
