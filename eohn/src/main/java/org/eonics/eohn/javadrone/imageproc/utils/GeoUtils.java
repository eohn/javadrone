/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.utils;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.eonics.eohn.javadrone.imageproc.helpers.Vector2D;

/**
 *
 * @author JP
 */
public class GeoUtils {
    
    
    public static Vector2D calculateAverageVector(Collection<Vector2D> vectorCollection) {
        double queueSize = vectorCollection.size();
        double totalAngleInDegrees = 0;
        double totalDistance = 0;
        for(Vector2D vector2D: vectorCollection) {
            totalAngleInDegrees += get360DegreeAngle(vector2D.getAngleInDegrees());
            totalDistance += vector2D.getDistance();
        }
        return new Vector2D(Math.toRadians(get180DegreeAngle(totalAngleInDegrees / queueSize)),
                totalDistance / queueSize);
    }

    public static  double get180DegreeAngle(double n360DegreeAngle) {
        if (n360DegreeAngle > 180) {
            return n360DegreeAngle - 360;
        }
        return n360DegreeAngle;
    }

    public static double get360DegreeAngle(double n180DegreeAngle) {
        if (n180DegreeAngle < 0) {
            return 360 + n180DegreeAngle;
        }
        return n180DegreeAngle;
    }

    public static double distance(Point p1, Point p2) {
        if(p1 == null || p2 == null) {
            return 0;
        }
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    public static double distance(Point2D p1, Point2D p2) {
        return distance(fromPoint2D(p1), fromPoint2D(p2));
    }

    public static double calculateRadians(Point2D p1, Point2D p2) {
        double xDiff = p1.getX() - p2.getX();
        // prevent division by 0
        if (xDiff == 0) {
            xDiff = 0.0000000000001D;
        }
        return Math.atan2((p1.getY() - p2.getY()), xDiff);
    }

    public static double calculateRadians(Line2D line) {
        if (line != null) {
            return calculateRadians(line.getP1(), line.getP2());
        }
        return 0.0D;
    }

    public static Point fromPoint2D(Point2D p2d) {
        if(p2d == null) {
            return null;
        }
        return new Point((int) p2d.getX(), (int) p2d.getY());
    }

    public static List<Rectangle> combineOverlaps(List<Rectangle> rectangles) {
        List<Rectangle> nBlobs = new ArrayList<>();
        Set<Rectangle> processedBlobs = new HashSet<>();
        for (Rectangle blob : rectangles) {
            if (!processedBlobs.contains(blob)) {
                Rectangle resultBlob = blob.getBounds();
                for (Rectangle blob1 : rectangles) {
                    if (blob != blob1 && !processedBlobs.contains(blob1)) {
                        if (blob.intersects(blob1)) {
                            resultBlob.union(blob1);
                            processedBlobs.add(blob1);
                        }
                    }
                }
                nBlobs.add(resultBlob);
            }
        }
        return nBlobs;
    }
}
