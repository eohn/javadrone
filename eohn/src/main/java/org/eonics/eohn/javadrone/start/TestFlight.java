package org.eonics.eohn.javadrone.start;

import com.codeminders.ardrone.ARDrone;

import org.eonics.eohn.javadrone.behaviour.DroneAction;
import org.eonics.eohn.javadrone.behaviour.DroneMotion;
import org.eonics.eohn.javadrone.behaviour.MoveToVector;
import org.eonics.eohn.javadrone.dataproc.redline.RedLineDataProcessor;

import org.eonics.eohn.javadrone.gui.DroneDashboard;
import org.eonics.eohn.javadrone.imageproc.colorbased.redline.RedLineImageProcessor;
import org.opencv.core.Core;

public class TestFlight {

    private static final long CONNECT_TIMEOUT = 3000;

    /**
     * a
     *
     * @param args
     */
    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        try {
            ARDrone drone = new ARDrone();
            drone.connect();
            drone.clearEmergencySignal();

            drone.selectVideoChannel(ARDrone.VideoChannel.VERTICAL_ONLY);

            MoveToVector moveToVector = new MoveToVector(drone);

            RedLineImageProcessor redLineImageProcessor = new RedLineImageProcessor();
            redLineImageProcessor.addDataListener(new RedLineDataProcessor(drone, moveToVector));
            DroneDashboard dd = new DroneDashboard(redLineImageProcessor);
            dd.setDrone(drone);

            // Wait until drone is ready
            drone.waitForReady(3000);

            // do TRIM operation
            drone.trim();

            // Take off
            System.err.println("Taking off");
            drone.takeOff();

            Thread.sleep(5000);
            System.out.println("Staring motions");

            System.out.println("Moving left");
            for (int i = 0; i < 10; i++) {
                new DroneMotion(drone, DroneAction.TURN_LEFT, 0.3F).execute();
                Thread.sleep(200);
            }
//            System.out.println("Moving right");            
//            for (int i = 0; i < 20; i++) {
//                new DroneMotion(drone, DroneAction.MOVE_LEFT, 0.1F).execute();
//                Thread.sleep(100);
//            }
//            
//            System.out.println("Turning left");            
//            for (int i = 0; i < 20; i++) {
//                new DroneMotion(drone, DroneAction.TURN_LEFT, 0.1F).execute();
//                Thread.sleep(100);
//            }
//            
//            System.out.println("Turning right");            
//            for (int i = 0; i < 20; i++) {
//                new DroneMotion(drone, DroneAction.TURN_RIGHT, 0.1F).execute();
//                Thread.sleep(100);
//            }
//            
//            System.out.println("Moving forward");            
//            for (int i = 0; i < 20; i++) {
//                new DroneMotion(drone, DroneAction.MOVE_FORWARD, 0.1F).execute();
//                Thread.sleep(100);
//            }
//            
//            System.out.println("Moving backward");            
//            for (int i = 0; i < 10; i++) {
//                new DroneMotion(drone, DroneAction.MOVE_BACKWARD, 0.1F).execute();
//                Thread.sleep(100);
//            }
            
            Thread.sleep(5000);
            drone.land();

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
