package org.eonics.eohn.javadrone.imageproc.matrixCodes;

import org.eonics.eohn.javadrone.ImageData;

import java.awt.geom.Point2D;

public class MatrixCodeImageData implements ImageData {

    private Point2D matrixCenter;
    private String content;
    private int screenWidth;
    private int screenHeight;
    private double matrixSize;

    public MatrixCodeImageData(Point2D matrixCenter, String content, int screenWidth, int screenHeight, double matrixSize)
    {
        this.matrixCenter = matrixCenter;
        this.content = content;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.matrixSize = matrixSize;
    }

    public Point2D getMatrixCenter() {

        return matrixCenter;
    }

    public String getContent() {
        return content;
    }

    public boolean matrixPresent(){
        return matrixCenter != null;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public Point2D getScreenCenter(){
        return new Point2D.Double(screenWidth/2, screenHeight/2);
    }

    public double getMatrixSize() {
        return matrixSize;
    }
}
