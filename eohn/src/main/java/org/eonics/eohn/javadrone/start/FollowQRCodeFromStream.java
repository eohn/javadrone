package org.eonics.eohn.javadrone.start;

import org.eonics.eohn.javadrone.gui.DroneDashboard;
import org.eonics.eohn.javadrone.imageproc.matrixCodes.MatrixCodeImageProcessor;
import org.eonics.eohn.javadrone.imageproc.utils.VideoStream;
import org.opencv.core.Core;

public class FollowQRCodeFromStream {

    public static void main(String[] args){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        try {

            MatrixCodeImageProcessor imageProcessor = new MatrixCodeImageProcessor();
            DroneDashboard droneDashboard = new DroneDashboard(imageProcessor);

            // Start stream on RPi (using command below) before running the program!
            // raspivid -t 0 -n -lev 4.2 -w 640 -h 480 -hf -fps 15 -l -o tcp://0.0.0.0:3333
            VideoStream.streamToImageView("192.168.0.102", 3333, 0, "h264", 15, 1000, "ultrafast", 0);

        } catch (Throwable e) {
            e.printStackTrace();
        }



    }
}
