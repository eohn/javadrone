package org.eonics.eohn.javadrone.start;

import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.DroneMovementController;
import org.eonics.eohn.javadrone.dataproc.StateDataHandler;
import org.eonics.eohn.javadrone.gui.ControlPanel;
import org.eonics.eohn.javadrone.gui.DroneDashboard;
import org.eonics.eohn.javadrone.imageproc.colorbased.redline.RedLineImageProcessor;
import org.eonics.eohn.javadrone.imageproc.utils.ImgProcUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import java.awt.image.BufferedImage;

public class DroneTrackRedLine {

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        try {
            
        	ARDrone drone = new ARDrone();
            drone.connect();
            drone.clearEmergencySignal();

            drone.selectVideoChannel(ARDrone.VideoChannel.VERTICAL_ONLY);

            // Wait until drone is ready
            drone.waitForReady(3000);

            // do TRIM operation
            drone.trim();

            // Take off
            System.err.println("Taking off");
            drone.takeOff();

            RedLineImageProcessor redLineImageProcessor = new RedLineImageProcessor();

            redLineImageProcessor.addDataListener(StateDataHandler.getInstance());

            DroneDashboard droneDashboard = new DroneDashboard(redLineImageProcessor);
            droneDashboard.setDrone(drone);
            new ControlPanel(drone);

            Thread.sleep(5000);

            new DroneMovementController(drone);

            Mat frame = new Mat();

            long currentTimeMillis = System.currentTimeMillis();

            while (System.currentTimeMillis() - currentTimeMillis < 1000000) {
                BufferedImage mat2Img = ImgProcUtils.mat2Img(frame);
                if (mat2Img != null) {
                    droneDashboard.getVideoPanel().imageReceived(mat2Img);
                    droneDashboard.getConvolvingVideoPanel().imageReceived(mat2Img);
                }
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
