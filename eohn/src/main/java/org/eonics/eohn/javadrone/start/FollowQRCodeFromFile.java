package org.eonics.eohn.javadrone.start;
import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.DroneMovementController;
import org.eonics.eohn.javadrone.DroneQRMovementController;
import org.eonics.eohn.javadrone.gui.DroneDashboard;

import org.eonics.eohn.javadrone.imageproc.matrixCodes.MatrixCodeImageProcessor;
import org.eonics.eohn.javadrone.imageproc.matrixCodes.QRDataHandler;
import org.eonics.eohn.javadrone.imageproc.utils.ImgProcUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import static org.mockito.Mockito.mock;

import java.awt.image.BufferedImage;

public class FollowQRCodeFromFile {

    public static void main(String[] args) throws InterruptedException {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        ARDrone mockDrone = mock(ARDrone.class);

        VideoCapture camera = new VideoCapture("/home/djura/Videos/20180309_165245.mp4");

        MatrixCodeImageProcessor processor = new MatrixCodeImageProcessor();
        processor.addDataListener(QRDataHandler.getInstance());

        DroneDashboard dd = new DroneDashboard(processor);

        Mat frame = new Mat();

        long currentTimeMillis = System.currentTimeMillis();

        new DroneQRMovementController(mockDrone);

        while (System.currentTimeMillis() - currentTimeMillis < 120000) {
            camera.read(frame);

            BufferedImage mat2Img = ImgProcUtils.mat2Img(frame);

            System.out.println(mat2Img);
            if (mat2Img != null) {
                dd.getVideoPanel().imageReceived(mat2Img);
                dd.getConvolvingVideoPanel().imageReceived(mat2Img);
                Thread.sleep(20);
            }
        }

    }
}
