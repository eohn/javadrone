/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.helpers;

import java.awt.Point;

/**
 *
 * @author JP
 */
public class PointsAverage {

    private long sumX = 0;
    private long sumY = 0;
    private int countX = 0;
    private int countY = 0;

    private synchronized void addX(int x) {
        sumX += x;
        countX++;
    }

    private synchronized void addY(int y) {
        sumY += y;
        countY++;
    }
    
    public synchronized void add(int x, int y) {
        addX(x);
        addY(y);
    }

    public Point getAverage() {
        return new Point((int) (sumX / getCountX()), (int) (sumY / getCountY()));
    }

    /**
     * @return the countX
     */
    public int getCountX() {
        return countX;
    }

    /**
     * @return the countY
     */
    public int getCountY() {
        return countY;
    }
}
