package com.codeminders.ardrone.data.decoder.ardrone20;

import java.awt.image.BufferedImage;
import java.util.logging.Logger;

import com.codeminders.ardrone.ARDrone;
import com.codeminders.ardrone.VideoDataDecoder;
import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.Utils;

public class ARDrone20VideoDataDecoder extends VideoDataDecoder {

	private final Logger log = Logger.getLogger(this.getClass().getName());
	private boolean done = false;

	public ARDrone20VideoDataDecoder(ARDrone drone) {
		super(drone);
		setName("ARDrone 2.0 Video decoding thread");
	}

	@Override
	public void finish() {
		done = true;
	}

	@Override
	public void run() {
		super.run();

		while (!done) {
			pauseCheck();
			decode();
		}

		log.fine("Video Decodding thread is stopped");
	}

	private void decode() {

		IContainer container = IContainer.make();

		BufferedImage img = null;

		if (container.open(getDataReader().getDataStream(), null) < 0) {
			throw new IllegalArgumentException("could not open inpustream");
		}

		final int numStreams = container.getNumStreams();

		int videoStreamId = -1;
		IStreamCoder videoCoder = null;
		for (int i = 0; i < numStreams; i++) {
			final IStream stream = container.getStream(i);
			final IStreamCoder coder = stream.getStreamCoder();

			if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
				videoStreamId = i;
				videoCoder = coder;
				break;
			}
		}
		if (videoStreamId == -1) {
			throw new RuntimeException("could not find video stream");
		}

		if (videoCoder.open() < 0) {
			throw new RuntimeException(
					"could not open video decoder for container");
		}

		IVideoResampler resampler = null;
		if (videoCoder.getPixelType() != IPixelFormat.Type.BGR24) {
			resampler = IVideoResampler.make(videoCoder.getWidth(),
					videoCoder.getHeight(), IPixelFormat.Type.BGR24,
					videoCoder.getWidth(), videoCoder.getHeight(),
					videoCoder.getPixelType());
			if (resampler == null) {
				throw new RuntimeException(
						"could not create color space resampler.");
			}
		}

		final IPacket packet = IPacket.make();
		long firstTimestampInStream = Global.NO_PTS;
		long systemClockStartTime = 0;
		while (container.readNextPacket(packet) >= 0) {
			if (packet.getStreamIndex() == videoStreamId) {
				final IVideoPicture picture = IVideoPicture.make(
						videoCoder.getPixelType(), videoCoder.getWidth(),
						videoCoder.getHeight());

				try {
					int offset = 0;
					while (offset < packet.getSize()) {
						final int bytesDecoded = videoCoder.decodeVideo(
								picture, packet, offset);
						if (bytesDecoded < 0) {
							throw new RuntimeException(
									"got error decoding video");
						}
						offset += bytesDecoded;

						if (picture.isComplete()) {
							IVideoPicture newPic = picture;
							if (resampler != null) {
								newPic = IVideoPicture
										.make(resampler.getOutputPixelFormat(),
												picture.getWidth(),
												picture.getHeight());
								if (resampler.resample(newPic, picture) < 0) {
									throw new RuntimeException(
											"could not resample video");
								}
							}
							if (newPic.getPixelType() != IPixelFormat.Type.BGR24) {
								throw new RuntimeException(
										"could not decode video as BGR 24 bit data");
							}

							if (firstTimestampInStream == Global.NO_PTS) {
								firstTimestampInStream = picture.getTimeStamp();
								systemClockStartTime = System
										.currentTimeMillis();
							} else {
								final long systemClockCurrentTime = System
										.currentTimeMillis();
								final long millisecondsClockTimeSinceStartofVideo = systemClockCurrentTime
										- systemClockStartTime;

								final long millisecondsStreamTimeSinceStartOfVideo = (picture
										.getTimeStamp() - firstTimestampInStream) / 1000;
								final long millisecondsTolerance = 50;
								final long millisecondsToSleep = (millisecondsStreamTimeSinceStartOfVideo - (millisecondsClockTimeSinceStartofVideo + millisecondsTolerance));
								if (millisecondsToSleep > 0) {
									try {
										Thread.sleep(millisecondsToSleep);
									} catch (final InterruptedException e) {
									}
								}
							}

							img = Utils.videoPictureToImage(newPic);
							notifyDroneWithDecodedFrame(
									0,
									0,
									img.getWidth(),
									img.getHeight(),
									
									img.getRGB(0, 0, img.getWidth(),
											img.getHeight(), null, 0,
											img.getWidth()), 0, img.getWidth());

						}
					}
				} catch (final Exception exc) {
					exc.printStackTrace();
				}
			} else {
				do {
				} while (false);
			}

		}

		if (videoCoder != null) {
			videoCoder.close();
			videoCoder = null;
		}
		if (container != null) {
			container.close();
			container = null;
		}
	}

}