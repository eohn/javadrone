/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.dataproc.redline;

import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.ImageProcessorDataListener;
import org.eonics.eohn.javadrone.behaviour.MoveToVector;
import org.eonics.eohn.javadrone.imageproc.colorbased.redline.RedLineImageData;
import org.eonics.eohn.javadrone.imageproc.helpers.Vector2D;

/**
 *
 * @author JP
 */
public class RedLineDataProcessor implements ImageProcessorDataListener<RedLineImageData> {

    private final MoveToVector moveToVector;
    private ARDrone drone;

    public RedLineDataProcessor(ARDrone drone, MoveToVector moveToVector) {
        this.moveToVector = moveToVector;
        this.drone = drone;
    }

    @Override
    public void processImageData(RedLineImageData data) {
        if (data != null && data.getClosestPoint() != null && !data.isLineInCenter()) {            
            Vector2D vectorToClosestPoint = data.getVectorToClosestPoint();
            System.out.println("Adding vector " + vectorToClosestPoint);
            moveToVector.addVector(vectorToClosestPoint);
            if (moveToVector.isStopped()) {
                moveToVector.start();
            }
        } else if (moveToVector != null) {
//            try {
                //System.out.println("Stop motion");
                moveToVector.stop();
//                drone.hover();
                if (data != null) {
                    System.out.println("Angle: " + Math.toDegrees(data.getLineAngleInRadians()));
                }
//            } catch (IOException ex) {
//                Logger.getLogger(RedLineDataProcessor.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }
}
