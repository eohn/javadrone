package org.eonics.eohn.javadrone.start;


import java.awt.image.BufferedImage;

import org.eonics.eohn.javadrone.behaviour.MoveToVector;
import org.eonics.eohn.javadrone.dataproc.redline.RedLineDataProcessor;

import org.eonics.eohn.javadrone.gui.DroneDashboard;
import org.eonics.eohn.javadrone.gui.ControlPanel;
import org.eonics.eohn.javadrone.imageproc.utils.ImgProcUtils;
import org.eonics.eohn.javadrone.imageproc.colorbased.redline.RedLineImageProcessor;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

public class RedFilteringFromVideoFile {

    private static final long CONNECT_TIMEOUT = 3000;

    /**
     * a
     *
     * @param args
     */
    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        try {
            
            MoveToVector moveToVector = new MoveToVector(null);
            //ControlPanel controlPanel = new ControlPanel(null, moveToVector);

            Thread.sleep(5000);

//            VideoCapture camera = new VideoCapture("/Users/JP/Downloads/VIDEO-0007.avi");
//            VideoCapture camera = new VideoCapture("/Users/JP/Downloads/VIDEO-0007.avi");
//            VideoCapture camera = new VideoCapture("/Users/JP/Downloads/VIDEO-0007.avi");
            VideoCapture camera = new VideoCapture("/home/djura/Videos/Nippon/Picamera/9.h264");
            
            RedLineImageProcessor redLineImageProcessor = new RedLineImageProcessor();
            //redLineImageProcessor.addDataListener(new RedLineDataProcessor(null, moveToVector));
            DroneDashboard dd = new DroneDashboard(redLineImageProcessor);

            Mat frame = new Mat();

            long currentTimeMillis = System.currentTimeMillis();
            while (System.currentTimeMillis() - currentTimeMillis < 60000) {
                camera.read(frame);
                
                BufferedImage mat2Img = ImgProcUtils.mat2Img(frame);

                System.out.println(mat2Img);
                if (mat2Img != null) {
                    dd.getVideoPanel().imageReceived(mat2Img);
                    dd.getConvolvingVideoPanel().imageReceived(mat2Img);
                    //Thread.sleep(50);
                }
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
