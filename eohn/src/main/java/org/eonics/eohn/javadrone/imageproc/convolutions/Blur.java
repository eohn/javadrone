/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.convolutions;

import org.eonics.eohn.javadrone.OpenCvConvolution;
import static java.lang.System.out;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author JP
 */
public class Blur extends OpenCvConvolution {

    @Override
    public Mat convolve(Mat in) {
        Mat out = new Mat(in.rows(), in.cols(), in.type());
        Imgproc.blur(in, out, new Size(45, 45));

        return out;
    }

}
