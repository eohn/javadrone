package org.eonics.eohn.javadrone;

import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.behaviour.*;
import org.eonics.eohn.javadrone.config.DroneConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DroneMovementController implements Runnable {

    private ARDrone drone;

    private Thread t;

    private LandMovementBehaviour landMovementBehaviour = new LandMovementBehaviour();

    private List<MovementBehaviour> movementBehaviours = new ArrayList<>();

    public DroneMovementController(ARDrone drone) {
        this.drone = drone;

        movementBehaviours.add(new RotationalMovementBehaviour());
        movementBehaviours.add(new LongitudinalMovementBehaviour());
        movementBehaviours.add(new LateralMovementBehaviour());
        movementBehaviours.add(new VerticalMovementBehaviour());

        start();
    }

    public synchronized void start() {
        System.out.println("starting new thread");
        t = new Thread(this);
        t.start();
    }

    public Movement queryMovementBehaviours() {
        if (isEmergencyLandingRequired()) {
            return new Movement();
        }

        return movementBehaviours.stream()
                .map(MovementBehaviour::query)
                .reduce(Movement::combine)
                .orElse(new Movement());
    }

    private boolean isEmergencyLandingRequired() {
        Movement land = landMovementBehaviour.query();

        if (land.getVerticalVelocity() == Float.NEGATIVE_INFINITY) {
            try {
                t = null;
                drone.land();
                System.out.println("EMERGENCY LANDING!");
            } catch (IOException e) {
                e.printStackTrace();

                try {
                    drone.sendEmergencySignal();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public void run() {
        while (t != null) {
            try {
                Movement movement = queryMovementBehaviours();
                executeMovement(movement);

                Thread.sleep(DroneConfig.threadSleepTime);
            } catch (InterruptedException | IOException ex) {
                Logger.getLogger(DroneMovementController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private void executeMovement(Movement m) throws IOException {
        if (m.getLateralTilt() != 0 || m.getLongitudinalTilt() != 0 || m.getVerticalVelocity() != 0 || m.getAngularVelocity() != 0) {
            drone.move(
                (float) m.getLateralTilt(),
                (float) m.getLongitudinalTilt(),
                (float) m.getVerticalVelocity(),
                (float) m.getAngularVelocity()
            );

            System.out.println(m.toString());
        }
    }

}
