/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.redline;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import org.eonics.eohn.javadrone.ImageData;
import org.eonics.eohn.javadrone.imageproc.helpers.Vector2D;
import static org.eonics.eohn.javadrone.imageproc.utils.GeoUtils.*;

/**
 *
 * @author JP
 */
public class RedLineImageData implements ImageData {

    private Point2D center;
    private Point2D closestPoint;
    private CircleCrossings circleCrossings;

    public RedLineImageData(Point2D center, Point2D closestPoint, CircleCrossings circleCrossings) {
        this.center = center;
        this.closestPoint = closestPoint;
        this.circleCrossings = circleCrossings;
    }

    public Vector2D getVectorToClosestPoint() {
        if(closestPoint == null) {
            return null;
        }
        return new Vector2D(calculateRadians(center, closestPoint),
                distance(center, closestPoint));
    }

    public Double getLineAngleInRadians() {
        if (circleCrossings.getCrossingLine() == null) {
            return null;
        }
        return calculateRadians(circleCrossings.getCrossingLine());
    }

    public Double getLineAngleInDegrees() {
        if (circleCrossings.getCrossingLine() == null) {
            return null;
        }
        return Math.toDegrees(getLineAngleInRadians());
    }
    
    public boolean closestPointIsInCircle() {
        return circleCrossings.pointIsInCircle(fromPoint2D(closestPoint));
    }
    
    public boolean isLineInCenter() {
        return distance(closestPoint, center) < 200;
    }
    
    public Line2D getLineToClosestPoint() {
        return new Line2D.Double(center, closestPoint);
    }

    /**
     * @return the center
     */
    public Point2D getCenter() {
        return center;
    }

    /**
     * @param center the center to set
     */
    public void setCenter(Point2D center) {
        this.center = center;
    }

    /**
     * @return the closestPoint
     */
    public Point2D getClosestPoint() {
        return closestPoint;
    }

    /**
     * @param closestPoint the closestPoint to set
     */
    public void setClosestPoint(Point2D closestPoint) {
        this.closestPoint = closestPoint;
    }

    /**
     * @return the circleCrossings
     */
    public CircleCrossings getCircleCrossings() {
        return circleCrossings;
    }

    /**
     * @param circleCrossings the circleCrossings to set
     */
    public void setCircleCrossings(CircleCrossings circleCrossings) {
        this.circleCrossings = circleCrossings;
    }

}
