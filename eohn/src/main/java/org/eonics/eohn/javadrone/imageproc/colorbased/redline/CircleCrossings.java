/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.redline;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import static org.eonics.eohn.javadrone.imageproc.utils.GeoUtils.distance;

/**
 *
 * @author JP
 */
public class CircleCrossings {

    private final static int CIRCLE_EDGE_WIDTH = 10;
    private final static int CIRCLE_RADIUS = 160;

    private int minXCrossing = Integer.MAX_VALUE;
    private int maxXCrossing = 0;
    private int minYCrossing = Integer.MAX_VALUE;
    private int maxYCrossing = 0;

    private Point minXCrossingPoint = null;
    private Point minYCrossingPoint = null;
    private Point maxXCrossingPoint = null;
    private Point maxYCrossingPoint = null;

    private Point closestCrossingPoint, furthestCrossingPoint, remainingCrossingPoint;

    private final Point center;
    private boolean crossingEdgePointsCalculated = false;

    public CircleCrossings(Point center) {
        this.center = center;
    }

    public void processPixelAt(Point pixel) {       

        if (pointIsOnCircle(pixel)) {
            if (pixel.x < minXCrossing) {
                minXCrossing = pixel.x;
                minXCrossingPoint = pixel;
            }
            if (pixel.y < minYCrossing) {
                minYCrossing = pixel.y;
                minYCrossingPoint = pixel;
            }
            if (pixel.x > maxXCrossing) {
                maxXCrossing = pixel.x;
                maxXCrossingPoint = pixel;
            }
            if (pixel.y > maxYCrossing) {
                maxYCrossing = pixel.y;
                maxYCrossingPoint = pixel;
            }
        }
    }
    
    public boolean pointIsOnCircle(Point p) {
        double distance = distance(p, center);
        return distance < CIRCLE_RADIUS + CIRCLE_EDGE_WIDTH / 2
                && distance > CIRCLE_RADIUS - CIRCLE_EDGE_WIDTH / 2;
    }
    
    public boolean pointIsInCircle(Point p) {
        if(p == null) {
            return false;
        }
        return distance(p, center) < CIRCLE_RADIUS;
    }

    public Line2D getCrossingLine() {
        calculateCrossingEdgePoints();

        if(!pointsAreValid()) {
            return null;
        }

        Point2D.Double lineStart = calcLineStart();
        Point2D.Double lineEnd = calcLineEnd();

        if (lineStart == null || lineEnd == null) {
            return null;
        }
        return new Line2D.Double(lineStart, lineEnd);
    }

    private synchronized void calculateCrossingEdgePoints() {
        if (minXCrossingPoint != null && (minXCrossingPoint == minYCrossingPoint)) {
            System.out.println("minXCrossingPoint == minYCrossingPoint");
        }

        if (!crossingEdgePointsCalculated) {
            crossingEdgePointsCalculated = true;

            Point[] points = new Point[]{getMaxXCrossingPoint(), getMaxYCrossingPoint(), getMinXCrossingPoint(), getMinYCrossingPoint()};

            double minDistance = Double.MAX_VALUE;
            double maxDistance = 0;
            double d;

            closestCrossingPoint = null;
            furthestCrossingPoint = null;
            remainingCrossingPoint = null;

            for (Point p : points) {
                if (p != getMinXCrossingPoint()) {
                    d = distance(p, getMinXCrossingPoint());

                    if (d < minDistance) {
                        minDistance = d;
                        closestCrossingPoint = p;
                    }

                    if (d > maxDistance) {
                        maxDistance = d;
                        furthestCrossingPoint = p;
                    }
                }
            }

            for (Point p : points) {
                if (p != getMinXCrossingPoint() && p != getClosestCrossingPoint() && p != getFurthestCrossingPoint()) {
                    remainingCrossingPoint = p;
                }
            }

            if(remainingCrossingPoint == null) {
                remainingCrossingPoint = furthestCrossingPoint;
            }
        }
    }

    private boolean pointsAreValid() {
        if(distance(minXCrossingPoint, closestCrossingPoint) > 50) {
            return false;
        }
        if(distance(furthestCrossingPoint, remainingCrossingPoint) > 50) {
            return false;
        }
        if(distance(minXCrossingPoint, furthestCrossingPoint) < 50) {
            return false;
        }
        if(distance(minXCrossingPoint, remainingCrossingPoint) < 50) {
            return false;
        }
        return true;
    }

    private Point2D.Double calcLineStart() {
        if (getMinXCrossingPoint() == null || getClosestCrossingPoint() == null) {
            return null;
        }
        return new Point2D.Double((getMinXCrossingPoint().x + getClosestCrossingPoint().x) / 2, (getMinXCrossingPoint().y + getClosestCrossingPoint().y) / 2);
    }

    private Point2D.Double calcLineEnd() {
        if (getFurthestCrossingPoint() == null || getRemainingCrossingPoint() == null) {
            return null;
        }
        return new Point2D.Double((getFurthestCrossingPoint().x + getRemainingCrossingPoint().x) / 2, (getFurthestCrossingPoint().y + getRemainingCrossingPoint().y) / 2);
    }

    /**
     * @return the minXCrossingPoint
     */
    public Point getMinXCrossingPoint() {
        return minXCrossingPoint;
    }

    /**
     * @return the minYCrossingPoint
     */
    public Point getMinYCrossingPoint() {
        return minYCrossingPoint;
    }

    /**
     * @return the maxXCrossingPoint
     */
    public Point getMaxXCrossingPoint() {
        return maxXCrossingPoint;
    }

    /**
     * @return the maxYCrossingPoint
     */
    public Point getMaxYCrossingPoint() {
        return maxYCrossingPoint;
    }

    /**
     * @return the closestCrossingPoint
     */
    public Point getClosestCrossingPoint() {
        return closestCrossingPoint;
    }

    /**
     * @return the furthestCrossingPoint
     */
    public Point getFurthestCrossingPoint() {
        return furthestCrossingPoint;
    }

    /**
     * @return the remainingCrossingPoint
     */
    public Point getRemainingCrossingPoint() {
        return remainingCrossingPoint;
    }

    public void setMinXCrossingPoint(Point minXCrossingPoint) {
        this.minXCrossingPoint = minXCrossingPoint;
    }
}
