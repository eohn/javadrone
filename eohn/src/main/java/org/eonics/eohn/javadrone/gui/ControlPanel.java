/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.gui;

import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.behaviour.DroneAction;
import org.eonics.eohn.javadrone.behaviour.DroneMotion;
import org.eonics.eohn.javadrone.config.DroneConfig;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;

import java.util.prefs.Preferences;

/**
 *
 * @author JP
 */
public class ControlPanel {

    private Preferences preferences;
    private static final String ACTION_MAP_KEY = "go";
    private ARDrone drone;

    private final JButton forward = new JButton("^");
    private final JButton backward = new JButton("v");
    private final JButton left = new JButton("<");
    private final JButton right = new JButton(">");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final JButton turnLeft = new JButton("t >");
    private final JButton turnRight = new JButton("< t");
    private final JButton land = new JButton("land");

    private JLabel maxAltitudeLabel = new JLabel();
    private JSlider maxAltitude = new JSlider();

    private JTextField hVelocityManualField = new JTextField();
    private JLabel hVelocityManualLabel = new JLabel();

    private JTextField vVelocityManualField = new JTextField();
    private JLabel vVelocityLabel = new JLabel();

    private JTextField aVelocityManualField = new JTextField();
    private JLabel aVelocityManualLabel = new JLabel();

    private JTextField hVelocityProgrammaticField = new JTextField();
    private JLabel hVelocityProgrammaticLabel = new JLabel();

    private JTextField aVelocityProgrammaticField = new JTextField();
    private JLabel aVelocityProgrammaticLabel = new JLabel();

    private JTextField threadSleepTimeProgrammaticField = new JTextField();
    private JLabel threadSleepTimeProgrammaticLabel = new JLabel();

    private JButton applyButtonManual = new JButton();
    private JButton applyButtonProgrammatic = new JButton();

    private float horizontalManualVelocity = 0.2F;
    private float verticalManualVelocity = 0.8F;
    private float angularManualVelocity = 0.25F;

    public ControlPanel(ARDrone drone) {
        this.drone = drone;

        preferences = Preferences.userNodeForPackage(this.getClass());
        initComponents();
        loadSettings();
    }

    private void initComponents() {
        JFrame frame = new JFrame();
        frame.setLayout(new GridLayout(4, 3));
        frame.setBounds(100, 800, 1000, 600);

        addDirectionalControlsToFrame(frame);

        frame.add(setupManualControlPanel());
        frame.add(setupProgrammaticPanel());

        addActionListeners();

        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private JPanel setupProgrammaticPanel() {
        JPanel programmaticPanel = new JPanel();
        programmaticPanel.setLayout(new GridLayout(4, 1));

        addHVelocityToProgrammaticPanel(programmaticPanel);
        addAVelocityToProgrammaticPanel(programmaticPanel);
        addThreadSleepTimeToProgrammaticPanel(programmaticPanel);
        addApplyButtonToProgrammaticPanel(programmaticPanel);
        return programmaticPanel;
    }

    private JPanel setupManualControlPanel() {
        JPanel manualPanel = new JPanel();
        manualPanel.setLayout(new GridLayout(5, 1));

        addHVelocityToManualPanel(manualPanel);
        addVVelocityToManualPanel(manualPanel);
        addAVelocityToManualPanel(manualPanel);
        addMaxAltitudeToManualPanel(manualPanel);
        addApplyButtonToManualPanel(manualPanel);
        return manualPanel;
    }

    private void addDirectionalControlsToFrame(JFrame frame) {
        frame.add(turnLeft);
        frame.add(forward);
        frame.add(turnRight);
        frame.add(left);
        frame.add(right);
        frame.add(down);
        frame.add(backward);
        frame.add(up);
        frame.add(land);
    }

    private void addThreadSleepTimeToProgrammaticPanel(JPanel programmaticPanel) {
        threadSleepTimeProgrammaticLabel.setText("Thread sleep time");
        programmaticPanel.add(threadSleepTimeProgrammaticLabel);
        programmaticPanel.add(threadSleepTimeProgrammaticField);
        threadSleepTimeProgrammaticField.setText(String.valueOf(DroneConfig.threadSleepTime));
    }

    private void addApplyButtonToProgrammaticPanel(JPanel programmaticPanel) {
        applyButtonProgrammatic.setText("Apply");
        programmaticPanel.add(applyButtonProgrammatic);
    }

    private void addApplyButtonToManualPanel(JPanel manualPanel) {
        applyButtonManual.setText("Apply");
        manualPanel.add(applyButtonManual);
    }

    private void addMaxAltitudeToManualPanel(JPanel manualPanel) {
        maxAltitudeLabel.setText("Max Altitude");
        maxAltitude.setMaximum(10000);
        manualPanel.add(maxAltitudeLabel);
        manualPanel.add(maxAltitude);
    }

    private void addHVelocityToProgrammaticPanel(JPanel programmaticPanel) {
        hVelocityProgrammaticLabel.setText("H-plane vel.");
        programmaticPanel.add(hVelocityProgrammaticLabel);
        programmaticPanel.add(hVelocityProgrammaticField);
        hVelocityProgrammaticField.setText(String.valueOf(DroneConfig.maxHorizontalVelocity));
    }

    private void addAVelocityToProgrammaticPanel(JPanel programmaticPanel) {
        aVelocityProgrammaticLabel.setText("Angular vel.");
        programmaticPanel.add(aVelocityProgrammaticLabel);
        programmaticPanel.add(aVelocityProgrammaticField);
        aVelocityProgrammaticField.setText(String.valueOf(DroneConfig.maxAngularVelocity));
    }

    private void addAVelocityToManualPanel(JPanel manualPanel) {
        aVelocityManualLabel.setText("Angular vel.");
        manualPanel.add(aVelocityManualLabel);
        manualPanel.add(aVelocityManualField);
        aVelocityManualField.setText(String.valueOf(getAngularManualVelocity()));
    }

    private void addVVelocityToManualPanel(JPanel manualPanel) {
        vVelocityLabel.setText("V-plane vel.");
        manualPanel.add(vVelocityLabel);
        manualPanel.add(vVelocityManualField);
        vVelocityManualField.setText(String.valueOf(getVerticalManualVelocity()));
    }

    private void addHVelocityToManualPanel(JPanel manualPanel) {
        hVelocityManualLabel.setText("H-plane vel.");
        manualPanel.add(hVelocityManualLabel);
        manualPanel.add(hVelocityManualField);
        hVelocityManualField.setText(String.valueOf(getHorizontalManualVelocity()));
    }

    private void addActionListeners() {
        left.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke('a'), ACTION_MAP_KEY);
        left.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.MOVE_LEFT, getAngularManualVelocity()).execute();
            }
        });
        
        forward.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke('w'), ACTION_MAP_KEY);
        forward.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.MOVE_FORWARD, getHorizontalManualVelocity()).execute();
            }
        });
        
        right.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke('d'), ACTION_MAP_KEY);
        right.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.MOVE_RIGHT, getAngularManualVelocity()).execute();
            }
        });
        
        turnLeft.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("LEFT"), ACTION_MAP_KEY);
        turnLeft.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.TURN_LEFT, getHorizontalManualVelocity()).execute();
            }
        });
        
        turnRight.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("RIGHT"), ACTION_MAP_KEY);
        turnRight.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.TURN_RIGHT, getHorizontalManualVelocity()).execute();
            }
        });
        
        down.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("DOWN"), ACTION_MAP_KEY);
        down.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.MOVE_DOWN, getVerticalManualVelocity()).execute();
            }
        });
        
        backward.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke('s'), ACTION_MAP_KEY);
        backward.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.MOVE_BACKWARD, getHorizontalManualVelocity()).execute();
            }
        });

        up.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("UP"), ACTION_MAP_KEY);
        up.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.MOVE_UP, getVerticalManualVelocity()).execute();
            }
        });

        land.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke('l'), ACTION_MAP_KEY);
        land.getActionMap().put(ACTION_MAP_KEY, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DroneMotion(drone, DroneAction.LAND, getVerticalManualVelocity()).execute();
            }
        });

        applyButtonManual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonManualActionPerformed(evt);
            }
        });

        applyButtonProgrammatic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonProgrammaticActionPerformed(evt);
            }
        });

        maxAltitude.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                updateMaxAltitude(evt);
            }
        });
    }

    private void applyButtonManualActionPerformed(ActionEvent evt) {
        setHorizontalManualVelocity(Float.valueOf(hVelocityManualField.getText()));
        setVerticalManualVelocity(Float.valueOf(vVelocityManualField.getText()));
        setAngularManualVelocity(Float.valueOf(aVelocityManualField.getText()));

        updateDrone();
        saveSettings();
    }

    private void applyButtonProgrammaticActionPerformed(ActionEvent evt) {
        setHorizontalProgrammaticVelocity(Float.valueOf(hVelocityProgrammaticField.getText()));
        setAngularProgrammaticVelocity(Float.valueOf(aVelocityProgrammaticField.getText()));
        setThreadSleepTime(Integer.valueOf(threadSleepTimeProgrammaticField.getText()));
    }

    private void updateMaxAltitude(ChangeEvent evt) {
        float value = (float) maxAltitude.getValue() / 1000.0f;
        maxAltitude.setToolTipText(value + "m");
        maxAltitudeLabel.setText("Max Altitude (" + value + "m)");
    }

    private synchronized void saveSettings() {
        preferences.putInt("control:altitude_max", maxAltitude.getValue());

        try {
            preferences.flush();
        } catch (BackingStoreException ex) {
            Logger.getLogger(ControlPanel.class.getName()).log(Level.SEVERE, "Cannot save settings: {0}", ex);
        }
    }

    private synchronized void loadSettings() {
        maxAltitude.setValue(preferences.getInt("control:altitude_max", 4000));
    }

    private synchronized void updateDrone() {
        if (drone == null) {
            return;
        }
        try {
            System.out.println("Setting max altitude");
            drone.setConfigOption("control:altitude_max", String.valueOf(maxAltitude.getValue()));
        } catch (IOException ex) {
            Logger.getLogger(ControlPanel.class.getName()).log(Level.SEVERE, "Exception Setting max altitude", ex);
        }
    }

    private void setHorizontalProgrammaticVelocity(float horizontalProgrammaticVelocity) {
        DroneConfig.maxHorizontalVelocity = horizontalProgrammaticVelocity;
    }

    private void setAngularProgrammaticVelocity(float angularProgrammaticVelocity) {
        DroneConfig.maxAngularVelocity = angularProgrammaticVelocity;
    }

    private void setThreadSleepTime(long threadSleepTime) {
        DroneConfig.threadSleepTime = threadSleepTime;
    }

    private float getHorizontalManualVelocity() {
        return this.horizontalManualVelocity;
    }

    private void setHorizontalManualVelocity(float horizontalManualVelocity) {
        this.horizontalManualVelocity = horizontalManualVelocity;
    }

    private float getVerticalManualVelocity() {
        return this.verticalManualVelocity;
    }

    private void setVerticalManualVelocity(float verticalManualVelocity) {
        this.verticalManualVelocity = verticalManualVelocity;
    }

    private float getAngularManualVelocity() {
        return this.angularManualVelocity;
    }

    private void setAngularManualVelocity(float angularManualVelocity) {
        this.angularManualVelocity = angularManualVelocity;
    }
}
