/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.redline;

import org.eonics.eohn.javadrone.ImageProcessor;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import org.eonics.eohn.javadrone.imageproc.helpers.ResultGrid;
import org.eonics.eohn.javadrone.imageproc.helpers.Screen;
import org.eonics.eohn.javadrone.imageproc.colorbased.colorfilters.RedLineFilter;
import org.eonics.eohn.javadrone.imageproc.utils.PaintUtils;

/**
 *
 * @author JP
 */
public class RedLineImageProcessor extends ImageProcessor<RedLineImageData> {

    @Override
    public BufferedImage convolve(BufferedImage in) {
        Screen screenIn = new Screen(in);

        BufferedImage out = new BufferedImage(screenIn.getWidth(), screenIn.getHeight(), BufferedImage.TYPE_INT_RGB);
        Screen screenOut = new Screen(out);

        RedLineFilter redlineFilter = new RedLineFilter(
                new ResultGrid(screenIn.getWidth(), screenIn.getHeight()));

        screenIn.filter(redlineFilter);

        ResultGrid resultGrid = redlineFilter.getResultGrid();

        RedLineResultGridProcessor redLineResultGridProcessor = new RedLineResultGridProcessor();
        RedLineImageData redLineImageData = redLineResultGridProcessor.processResultGrid(resultGrid);

        drawAxes(screenOut);
        resultGrid.paintOnScreen(screenOut, Color.red);

        if (redLineImageData != null) {
            drawClosestPoint(screenOut, redLineImageData);

            if (redLineImageData.isLineInCenter()) {
                drawAngleLine(screenOut, redLineImageData);
            } else {
                drawLine(screenOut, redLineImageData.getLineToClosestPoint());
                drawText(screenOut,
                        new Point((int) resultGrid.getCenter().getX() + 10, (int) resultGrid.getCenter().getY() - 10),
                        Math.toDegrees(redLineImageData.getVectorToClosestPoint().getAngleInRadians()) + " degrees");
            }
        }

        dataListeners.forEach((dataListener) -> {
            dataListener.processImageData(redLineImageData);
        });

        return out;
    }

    public void drawAngleLine(Screen screen, RedLineImageData redLineImageData) {
        if (redLineImageData != null) {
            CircleCrossings circleCrossings = redLineImageData.getCircleCrossings();

            Graphics2D g2d = screen.getGraphics2D();
            g2d.setColor(Color.white);
            final int pointRadius = 15;

            PaintUtils.drawCircle(g2d, circleCrossings.getMinXCrossingPoint(), 30, Color.BLUE);
            PaintUtils.drawCircle(g2d, circleCrossings.getClosestCrossingPoint(), pointRadius, Color.MAGENTA);
            PaintUtils.drawCircle(g2d, circleCrossings.getFurthestCrossingPoint(), pointRadius, Color.ORANGE);
            PaintUtils.drawCircle(g2d, circleCrossings.getRemainingCrossingPoint(), pointRadius, Color.WHITE);

            Line2D crossingLine = circleCrossings.getCrossingLine();
            if (crossingLine != null) {
                Point2D p1 = crossingLine.getP1();
                Point2D p2 = crossingLine.getP2();
                //PaintUtils.drawCircle(g2d, p1, pointRadius, Color.GREEN);
                //PaintUtils.drawCircle(g2d, p2, pointRadius, Color.YELLOW);
                drawLine(screen, crossingLine);
            }
        }
    }

    private void drawText(Screen screen, Point p, String txt) {
        screen.getGraphics2D().drawString(txt, p.x, p.y);
    }

    private void drawLine(Screen screen, Line2D line) {
        Graphics2D g2d = screen.getGraphics2D();
        g2d.setStroke(new BasicStroke(4));
        g2d.draw(new Line2D.Float((float) line.getX1(), (float) line.getY1(), (float) line.getX2(), (float) line.getY2()));
    }

    private void drawAxes(Screen screen) {
        int screenHeight = screen.getHeight();
        int screenWidth = screen.getWidth();

        int circleDiameter = (screenHeight > screenWidth) ? (screenWidth - 30) : (screenHeight - 30);

        Graphics2D g2d = screen.getGraphics2D();
        Point center = screen.getCenter();
        g2d.setColor(Color.white);
        g2d.drawLine(center.x, 0, center.x, screen.getHeight());
        g2d.drawLine(0, center.y, screen.getWidth(), center.y);

        g2d.drawOval(
            screen.getWidth() / 2 - circleDiameter / 2,
            screen.getHeight() / 2 - circleDiameter / 2,
            circleDiameter,
            circleDiameter
        );
    }

    private void drawClosestPoint(Screen screen, RedLineImageData redLineImageData) {
        if (redLineImageData != null && redLineImageData.getClosestPoint() != null) {
            PaintUtils.drawCircle(screen.getGraphics2D(), redLineImageData.getClosestPoint(), 20, Color.PINK);
        }
    }

}
