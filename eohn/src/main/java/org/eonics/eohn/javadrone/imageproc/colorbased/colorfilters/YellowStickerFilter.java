/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.colorfilters;

import org.eonics.eohn.javadrone.imageproc.colorbased.ColorFilter;
import org.eonics.eohn.javadrone.imageproc.helpers.ResultGrid;
import org.eonics.eohn.javadrone.imageproc.colorbased.colorscanners.YellowColorScanner;

/**
 *
 * @author JP
 */
public class YellowStickerFilter extends ColorFilter {

    public YellowStickerFilter(ResultGrid resultGrid) {
        super("yellowsticker", new YellowColorScanner(), resultGrid);
    }
}
