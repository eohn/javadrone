/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.convolutions;

import org.eonics.eohn.javadrone.OpenCvConvolution;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author JP
 */
public class Threshold extends OpenCvConvolution {

    private double minT;
    private double maxT;

    public Threshold(double minT, double maxT) {
        this.minT = minT;
        this.maxT = maxT;
    }

    @Override
    public Mat convolve(Mat in) {
        Mat out = new Mat(in.rows(), in.cols(), in.type());
        Imgproc.threshold(in, out, minT, maxT, Imgproc.THRESH_BINARY);
        return out;
    }

}
