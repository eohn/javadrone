/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.gui;

import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.ImageProcessor;
import org.eonics.eohn.javadrone.imageproc.utils.VideoStream;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 *
 * @author JP
 */
public class PiDroneDashboard extends JFrame {

    private final VideoPanel videoPanel = new VideoPanel();

    private final ConvolvingVideoPanel convolvingVideoPanel;
    private final NavDataPanel navDataPanel = new NavDataPanel();

    public PiDroneDashboard(ImageProcessor... convolutions) {
        super();
        convolvingVideoPanel = new ConvolvingVideoPanel(convolutions);
        initComponents();
        VideoStream.addDroneVideoListener(videoPanel);
        VideoStream.addDroneVideoListener(convolvingVideoPanel);
    }

    private void initComponents() {
        setBounds(100, 100, 1200, 400);
        setLayout(new GridLayout(1, 3));

        convolvingVideoPanel.setBackground(new Color(102, 102, 102));
        convolvingVideoPanel.setPreferredSize(new Dimension(640, 480));
        convolvingVideoPanel.setBorder(new LineBorder(Color.white));

        JPanel middle = new JPanel();
        middle.setPreferredSize(new Dimension(100, 240));

        videoPanel.setBackground(new Color(102, 102, 102));
        videoPanel.setPreferredSize(new Dimension(640, 480));
        videoPanel.setBorder(new LineBorder(Color.white));

        add(videoPanel);
//        add(middle);
        add(convolvingVideoPanel);
//        add(navDataPanel);

        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void setDrone(ARDrone drone) {
        //drone.addImageListener(videoPanel);
        //drone.addImageListener(convolvingVideoPanel);
        drone.addNavDataListener(navDataPanel);
    }

    public void dispose() {
        this.dispose();
    }

    public VideoPanel getVideoPanel() {
        return videoPanel;
    }

    public ConvolvingVideoPanel getConvolvingVideoPanel() {
        return convolvingVideoPanel;
    }
}
