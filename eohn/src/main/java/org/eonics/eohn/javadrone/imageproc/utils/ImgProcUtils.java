/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import static org.opencv.imgcodecs.Imgcodecs.*;

/**
 *
 * @author JP
 */
public final class ImgProcUtils {

    private static final Logger LOG = Logger.getLogger(ImgProcUtils.class.getName());

    public static BufferedImage mat2Img(Mat matrix) {
        BufferedImage bimg = null;
        if (matrix != null) {
            int cols = matrix.cols();
            int rows = matrix.rows();
            if (cols > 0 && rows > 0) {
                int elemSize = (int) matrix.elemSize();
                if (matrix.type() == CvType.CV_8UC1) {
                    byte[] data = new byte[cols * rows * elemSize];
                    matrix.get(0, 0, data);

                    bimg = new BufferedImage(cols, rows, BufferedImage.TYPE_BYTE_GRAY);
                    bimg.getRaster().setDataElements(0, 0, cols, rows, data);
                } else if (matrix.type() == CvType.CV_32FC3) {
                    float[] data = new float[cols * rows * elemSize];
                    matrix.get(0, 0, data);

                    bimg = new BufferedImage(cols, rows, BufferedImage.TYPE_INT_RGB);
                    bimg.getRaster().setDataElements(0, 0, cols, rows, convertFloatArray(data));
                } else {

                    MatOfByte matOfByte = new MatOfByte();

                    imencode(".jpg", matrix, matOfByte);

                    byte[] byteArray = matOfByte.toArray();

                    try {

                        InputStream in = new ByteArrayInputStream(byteArray);
                        bimg = ImageIO.read(in);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return bimg;
    }

    public static Mat img2Mat(BufferedImage src) {

        BufferedImage in = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g2d = in.createGraphics();
        g2d.drawImage(src, 0, 0, null);
        g2d.dispose();

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        int matType = -1;
        if (in.getType() == BufferedImage.TYPE_INT_RGB) {
            matType = CvType.CV_32FC3;
        } else if (in.getType() == BufferedImage.TYPE_BYTE_GRAY) {
            matType = CvType.CV_8UC1;
        }
        Mat mat = null;
        DataBuffer dataBuffer = in.getRaster().getDataBuffer();
        if (dataBuffer instanceof DataBufferInt) {
            mat = new Mat(in.getHeight(), in.getWidth(), matType);
            mat.put(0, 0, convertIntArray(((DataBufferInt) dataBuffer).getData()));
            LOG.info("Channels: " + mat.channels());
        } else if (dataBuffer instanceof DataBufferByte) {
            try {
                // mat.put(0, 0, ((DataBufferByte) dataBuffer).getData());

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(in, "jpg", baos);
                byte[] bytes = baos.toByteArray();

                mat = imdecode(new MatOfByte(bytes), CV_LOAD_IMAGE_UNCHANGED);
            } catch (IOException ex) {
                Logger.getLogger(ImgProcUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return mat;
    }

    private static float[] convertIntArray(int[] arr) {
        float[] fs = new float[arr.length];
        for (int i = 0; i < arr.length; i++) {
            fs[i] = (float) arr[i];
        }
        return fs;
    }

    private static int[] convertFloatArray(float[] arr) {
        int[] is = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            is[i] = (int) arr[i];
        }
        return is;
    }
}
