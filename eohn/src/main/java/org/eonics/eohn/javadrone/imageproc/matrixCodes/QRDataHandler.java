package org.eonics.eohn.javadrone.imageproc.matrixCodes;

import org.eonics.eohn.javadrone.ImageProcessorDataListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.geom.Point2D;

public class QRDataHandler implements ImageProcessorDataListener<MatrixCodeImageData> {

    static Logger LOGGER = LoggerFactory.getLogger(QRDataHandler.class);

    // How may frames a matrix code will be remembered
    private static final int MATRIX_EXPIRATION_FRAMES = 75;
    private int matrixExpires;

    private static QRDataHandler ourInstance;

    private Point2D qrCenter;

    private String matrixCodeText;

    private double matrixSize;

    private int screenWidth;

    private int screenHeight;



    public static QRDataHandler getInstance() {
        if (ourInstance == null) {
            ourInstance = new QRDataHandler();
        }

        return ourInstance;
    }


    @Override
    public void processImageData(MatrixCodeImageData data) {

        if(data.matrixPresent()) {
            LOGGER.info("New QR code location!");
            qrCenter = data.getMatrixCenter();
            matrixCodeText = data.getContent();

            // TODO: Will probably stay the same all the time, define these values only once somewhere else
            screenWidth = data.getScreenWidth();
            screenHeight = data.getScreenHeight();
            matrixExpires = MATRIX_EXPIRATION_FRAMES;
            matrixSize = data.getMatrixSize();
        } else{
            if(matrixExpires >= 0) {
                matrixExpires--;
            } else {
                clearMatrix();
            }
        }

    }

    public boolean matrixPresent(){
        return qrCenter != null;
    }

    private void clearMatrix(){
        qrCenter = null;
        matrixCodeText = null;
        matrixExpires = -1;
    }

    public Point2D getMatrixCenter() {
        return qrCenter;
    }
    public String getMatrixCodeText() {
        return matrixCodeText;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public double getMatrixSize() {
        return matrixSize;
    }
}
