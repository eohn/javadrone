/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.colorbased.colorscanners;

import java.awt.Color;
import org.eonics.eohn.javadrone.imageproc.colorbased.ColorScanner;
import org.eonics.eohn.javadrone.imageproc.utils.ColorUtils;

/**
 *
 * @author JP
 */
public class YellowColorScanner implements ColorScanner{

    @Override
    public boolean isColor(Color color) {
        int hue = ColorUtils.getHue(color);
        if(color.getBlue() > 60 && color.getBlue() < 80 && color.getRed() > 80 && color.getRed() < 100 && color.getGreen() > 87) {
//            System.out.println(color.getBlue());
            return true;
        }
        return false;
    }
    
}
