/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.helpers;

/**
 * @author JP
 */
public class Vector2D {
    
    private double angle;
    private double distance;

    public Vector2D(double angle, double distance) {
        this.angle = angle;
        this.distance = distance;
    }

    public double getAngleInDegrees() {
        return Math.toDegrees(angle);
    }

    /**
     * @return the angle
     */
    public double getAngleInRadians() {
        return angle;
    }

    /**
     * @param angle the angle to set
     */
    public void setAngle(double angle) {
        this.angle = angle;
    }

    /**
     * @return the distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Vector2D{" + "angle=" + angle + ", distance=" + distance + '}';
    }


}
