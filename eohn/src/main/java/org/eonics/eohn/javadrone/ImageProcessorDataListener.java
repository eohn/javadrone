/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone;

/**
 *
 * @author JP
 * @param <T>
 */
public interface ImageProcessorDataListener<T extends ImageData> {

    public abstract void processImageData(T data);
}
