/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JP
 * @param <T>
 */
public abstract class ImageProcessor<T extends ImageData> {

    protected List<ImageProcessorDataListener<T>> dataListeners = new ArrayList<>();

    public void addDataListener(ImageProcessorDataListener<T> dataListener) {
        dataListeners.add(dataListener);
    }

    public abstract BufferedImage convolve(BufferedImage in);
}
