package org.eonics.eohn.javadrone.config;

public class DroneConfig {

    public static double maxHorizontalVelocity = 0.06F;
    public static double maxAngularVelocity = 0.2F;

    public static long threadSleepTime = 60;

}
