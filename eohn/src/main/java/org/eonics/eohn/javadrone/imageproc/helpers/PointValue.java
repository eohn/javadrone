/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.helpers;

import java.awt.Point;

/**
 *
 * @author JP
 */
public class PointValue {

    private Point point;
    private int value;

    public PointValue(Point point, int value) {
        this.point = point;
        this.value = value;
    }

    public PointValue(int x, int y, int value) {
        this.point = new Point(x, y);
        this.value = value;
    }

    /**
     * @return the point
     */
    public Point getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(Point point) {
        this.point = point;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(int value) {
        this.value = value;
    }
}
