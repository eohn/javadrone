/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.behaviour;

import org.eonics.eohn.javadrone.Behaviour;
import com.codeminders.ardrone.ARDrone;
import java.io.IOException;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eonics.eohn.javadrone.imageproc.helpers.Vector2D;
import org.eonics.eohn.javadrone.imageproc.utils.GeoUtils;

/**
 *
 * @author JP
 */
public class MoveToVector extends Behaviour implements Runnable {

    private float horizontalVelocity = 0.05F;
    private float angularVelocity = 0.015F;

    private long threadSleepTime = 20;

    private final DroneMotionAngleMap droneMotionAngleMap = new DroneMotionAngleMap();
    private final Queue<Vector2D> vectorQueue = new ConcurrentLinkedQueue<>();

    private Vector2D[] queue = new Vector2D[3];
    private int index = 0;
    //    private Vector2D vector;
    private Thread t;
    private boolean stopped = true;

    public MoveToVector(ARDrone drone) {
        super(drone);
        droneMotionAngleMap.addAngleMapping((int angle) -> angle > 45 && angle <= 135,
                new DroneMotion(drone, DroneAction.MOVE_FORWARD, horizontalVelocity));
        droneMotionAngleMap.addAngleMapping((int angle) -> angle > -135 && angle <= -45,
                new DroneMotion(drone, DroneAction.MOVE_BACKWARD, horizontalVelocity));
        droneMotionAngleMap.addAngleMapping((int angle) -> angle > -45 && angle <= 45,
                new DroneMotion(drone, DroneAction.MOVE_LEFT, horizontalVelocity));
        droneMotionAngleMap.addAngleMapping((int angle) -> angle > 135 || angle <= -135,
                new DroneMotion(drone, DroneAction.MOVE_RIGHT, horizontalVelocity));
//        droneMotionAngleMap.addAngleMapping((int angle) -> (angle > 120 && angle <= 150) || (angle > -150 && angle <= -120),
//                new DroneMotion(drone, DroneAction.TURN_RIGHT, maxAngularVelocity));
//        droneMotionAngleMap.addAngleMapping((int angle) -> (angle > 30 && angle <= 60) || (angle > -60 && angle <= -30),
//                new DroneMotion(drone, DroneAction.TURN_LEFT, maxAngularVelocity));
    }

    @Override
    public synchronized void start() {
        System.out.println("starting new thread");
        stopped = false;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        System.out.println("Run: " + !isStopped());
        while (!isStopped()) {
            try {
                moveToVector();
                Thread.sleep(threadSleepTime);
            } catch (InterruptedException | IOException ex) {
                Logger.getLogger(MoveToVector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void moveToVector() throws IOException {
        Vector2D averageVector = calculateAverageVector();

        if (averageVector != null && averageVector.getAngleInDegrees() != Double.NaN ) {
            System.out.println("Moving to " + averageVector);
            droneMotionAngleMap.executeMotion((int) averageVector.getAngleInDegrees());
        } else {
            System.out.println("Warning, no vector to move to");
        }
    }

    private Vector2D calculateAverageVector() {        
//        int partialQueueSize = (int)(0.8 * (double)vectorQueue.size());
//        if(partialQueueSize == 0) {
//            return null;
//        }
//        List<Vector2D> l = new ArrayList<>();
//        for (int i = 0; i < partialQueueSize; i++) {
//            l.add(vectorQueue.poll());
//        }
        return GeoUtils.calculateAverageVector(Arrays.asList(queue));
    }

    @Override
    public synchronized void stop() {
        t = null;
        stopped = true;
    }

    public void addVector(Vector2D vector) {
//        vectorQueue.add(vector);
        queue[index] = vector;
        index++;
        if(index >= queue.length){
            index = 0;
        }
    }

    /**
     * @return the stopped
     */
    public boolean isStopped() {
        return stopped;
    }

    public float getHorizontalVelocity() {
        return horizontalVelocity;
    }

    public void setHorizontalVelocity(float horizontalVelocity) {
        this.horizontalVelocity = horizontalVelocity;
        System.out.println("Set horizontal velocity: " + this.horizontalVelocity);
    }

    public float getAngularVelocity() {
        return angularVelocity;
    }

    public void setAngularVelocity(float angularVelocity) {
        this.angularVelocity = angularVelocity;
        System.out.println("Set angular velocity: " + this.angularVelocity);
    }

    public long getThreadSleepTime() {
        return threadSleepTime;
    }

    public void setThreadSleepTime(long threadSleepTime) {
        this.threadSleepTime = threadSleepTime;
        System.out.println("Set thread sleep time: " + this.threadSleepTime);
    }
}
