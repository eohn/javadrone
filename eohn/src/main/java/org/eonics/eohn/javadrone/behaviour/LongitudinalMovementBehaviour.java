package org.eonics.eohn.javadrone.behaviour;

import org.eonics.eohn.javadrone.config.DroneConfig;
import org.eonics.eohn.javadrone.dataproc.StateDataHandler;

public class LongitudinalMovementBehaviour implements MovementBehaviour {

    public Movement query() {
        StateDataHandler state = StateDataHandler.getInstance();

        if(state.isDronePerpendicularToLine() && !state.isDroneLockedOnLine()) {
            double velocity;

            if (state.getDistance() < 150) {
                velocity = (double) Math.round(state.getDistance() / 150 * 100) // * 100 in order to round to X decimals
                    / 100 // in order to round to X decimals
                    * DroneConfig.maxHorizontalVelocity;
            } else {
                velocity = DroneConfig.maxHorizontalVelocity;
            }

            System.out.println("Distance: " + state.getDistance());
            System.out.println("H-velocity: " + velocity);

            if (state.getAngle() > 0) {
                return new Movement(0, - velocity, 0, 0);
            } else {
                return new Movement(0, velocity, 0, 0);
            }
        }

        return new Movement();
    }
}
