/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.imageproc.helpers;

import org.eonics.eohn.javadrone.imageproc.colorbased.ColorFilter;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 *
 * @author JP
 */
public class Screen {

    private final BufferedImage bufferedImage;
    private int width;
    private int height;

    public Screen(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
        this.width = bufferedImage.getWidth();
        this.height = bufferedImage.getHeight();
    }

    public Point getCenter() {
        return new Point(width / 2, height / 2);
    }

    public Graphics2D getGraphics2D() {
        return (Graphics2D) getBufferedImage().getGraphics();
    }

    public void filter(ColorFilter... colorFilters) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (ColorFilter colorFilter : colorFilters) {
                    colorFilter.filter(new Point(x, y),
                            new Color(bufferedImage.getRGB(x, y)));
                }
            }
        }
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the bufferedImage
     */
    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

}
