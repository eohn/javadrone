/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone.gui;

import java.awt.image.BufferedImage;
import org.eonics.eohn.javadrone.ImageProcessor;

/**
 *
 * @author JP
 */
public class ConvolvingVideoPanel extends VideoPanel {

    private ImageProcessor[] convolutions;

    public ConvolvingVideoPanel(ImageProcessor... convolutions) {
        this.convolutions = convolutions;
    }

    @Override
    public void frameReceived(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize) {
        BufferedImage im = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        im.setRGB(startX, startY, w, h, rgbArray, offset, scansize);
        imageReceived(im);
    }

    @Override
    public void imageReceived(BufferedImage bi) {
        for (ImageProcessor c : convolutions) {
            bi = c.convolve(bi);
        }
        super.imageReceived(bi); //To change body of generated methods, choose Tools | Templates.
    }

}
