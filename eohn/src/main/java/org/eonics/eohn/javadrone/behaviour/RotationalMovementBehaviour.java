package org.eonics.eohn.javadrone.behaviour;

import org.eonics.eohn.javadrone.config.DroneConfig;
import org.eonics.eohn.javadrone.dataproc.StateDataHandler;

public class RotationalMovementBehaviour implements MovementBehaviour {

    public Movement query() {
        StateDataHandler state = StateDataHandler.getInstance();
        double velocity;

        if(!state.isDronePerpendicularToLine() && !Double.isNaN(state.getAngle())) {

/*            if (Math.abs(state.getAngle()) > 78 && Math.abs(state.getAngle()) < 102) {
                velocity = (double) Math.round(
                        Math.pow(Math.abs(90 - Math.abs(state.getAngle())), 4) / 14000
                            * DroneConfig.maxAngularVelocity
                            * 100 // in order to round to X decimals
                )
                / 100; // in order to round to X decimals
            } else {
                velocity = DroneConfig.maxAngularVelocity;
            }*/

            if (state.getAngle() > 70 && state.getAngle() < 75 || state.getAngle() > 105 && state.getAngle() < 110) {
                velocity = DroneConfig.maxAngularVelocity - 0.05F;
            } else if (state.getAngle() >= 75 && state.getAngle() < 80 || state.getAngle() > 100 && state.getAngle() <= 105) {
                velocity = DroneConfig.maxAngularVelocity - 0.08F;
            } else if (state.getAngle() >= 80 && state.getAngle() <= 100) {
                velocity = DroneConfig.maxAngularVelocity - 0.12F;
            } else {
                velocity = DroneConfig.maxAngularVelocity;
            }

            //velocity = DroneConfig.maxAngularVelocity;

/*            System.out.println("Angle: " + state.getAngle());
            System.out.println("Angular velocity: " + velocity);*/

            if (state.getAngle() >= 0 && state.getAngle() <= 90) {
                return new Movement(0, 0, 0, - velocity);
            } else if (state.getAngle() > 90 && state.getAngle() <= 180) {
                return new Movement(0, 0, 0, velocity);
            } else if (state.getAngle() < 0 && state.getAngle() >= -90) {
                return new Movement(0, 0, 0, velocity);
            } else if (state.getAngle() < -90 && state.getAngle() >= -180) {
                return new Movement(0, 0, 0, - velocity);
            }
        }

        return new Movement();
    }
}
