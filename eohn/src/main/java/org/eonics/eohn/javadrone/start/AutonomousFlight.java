package org.eonics.eohn.javadrone.start;

import com.codeminders.ardrone.ARDrone;
import org.eonics.eohn.javadrone.gui.DroneDashboard;
import org.eonics.eohn.javadrone.imageproc.colorbased.JustRed;
import org.opencv.core.Core;

public class AutonomousFlight {

    private static final long CONNECT_TIMEOUT = 3000;

    /**a
     * @param args
     */
    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        
        
//        DroneDashboard dd = new DroneDashboard(new GrayScale(), new GaussianBlur(15), new Canny(5, 100));
//        DroneDashboard dd = new DroneDashboard(new Threshold(1, 5000));
//                , new GaussianBlur(), 
//                new Sobel());
//                
//                new GrayScale(), new Sobel());
        ARDrone drone;
        
        try {
            // Create ARDrone object,
            // connect to drone and initialize it.
            drone = new ARDrone();
            // drone.selectVideoChannel(ARDrone.VideoChannel.VERTICAL_ONLY);
            drone.connect();
            drone.clearEmergencySignal();

            DroneDashboard dd = new DroneDashboard(new JustRed(drone));
            dd.setDrone(drone);

            // Wait until drone is ready
            drone.waitForReady(CONNECT_TIMEOUT);

            // do TRIM operation
            drone.trim();
            
//            Thread.sleep(5000);

            // Take off
            System.err.println("Taking off");
            drone.takeOff();

            Thread.sleep(10000);
            // Fly a little :)
            long currentTimeMillis = System.currentTimeMillis();
            while (System.currentTimeMillis() - currentTimeMillis < 300000) {
//                drone.move(0, 0, 0, 1);
                Thread.sleep(2000);
            }
            // Land
            System.err.println("Landing");
            drone.land();

            // Give it some time to land
            Thread.sleep(2000);

            // Disconnect from the done
            drone.disconnect();

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
