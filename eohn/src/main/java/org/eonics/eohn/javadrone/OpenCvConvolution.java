/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.eohn.javadrone;

import org.eonics.eohn.javadrone.imageproc.utils.ImgProcUtils;
import java.awt.image.BufferedImage;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author JP
 */
public abstract class OpenCvConvolution extends ImageProcessor {

    @Override
    public final BufferedImage convolve(BufferedImage in) {
        Mat matSrc = ImgProcUtils.img2Mat(in);
        Mat matDest = new Mat();
        Imgproc.cvtColor(matSrc, matDest, Imgproc.COLOR_BGR2HSV);
        BufferedImage mat2Img = ImgProcUtils.mat2Img(convolve(matDest));
        return mat2Img;
    }

    public abstract Mat convolve(Mat in);
}
